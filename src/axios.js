import axios from 'axios'

const axiosInstance = axios.create({

 
    //  baseURL: 'http://10.80.1.94:8800',
     baseURL: 'http://localhost:5000',
 
    headers: {
        headerType: 'example header type'
    }
})

axiosInstance.interceptors.response.use(
    (response) => response,
    (error) =>
        Promise.reject(
            (error.response && error.response.data) || 'Something went wrong!'
        )
)

export default axiosInstance
