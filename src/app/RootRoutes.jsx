import React from 'react'
import { Redirect } from 'react-router-dom'

import dashboardRoutes from './views/dashboard/DashboardRoutes'
import utilitiesRoutes from './views/utilities/UtilitiesRoutes'

import materialRoutes from './views/material-kit/MaterialRoutes'
import chartsRoute from './views/charts/ChartsRoute'
import dragAndDropRoute from './views/Drag&Drop/DragAndDropRoute'
import invoiceRoutes from './views/invoice/InvoioceRoutes'
import calendarRoutes from './views/calendar/CalendarRoutes'
import crudRoute from './views/CRUD/CrudRoutes'
import inboxRoute from './views/inbox/InboxRoutes'
import formsRoutes from './views/forms/FormsRoutes'
import mapRoutes from './views/map/MapRoutes'
import chatRoutes from './views/chat-box/ChatRoutes'
import todoRoutes from './views/todo/TodoRoutes'
import pageLayoutRoutes from './views/page-layouts/PageLayoutRoutees'
import ListRoute from './views/list/ListRoute'

import pricingRoutes from './views/pricing/PricingRoutes'
import scrumBoardRoutes from './views/scrum-board/ScrumBoardRoutes'
import ecommerceRoutes from './views/ecommerce/EcommerceRoutes'
import pagesRoutes from './views/pages/pagesRoutes'
import dataTableRoutes from './views/data-table/dataTableRoutes'
import monitoringRoute from './views/APH_Clients/ClientsRoutes'
import RM_DoublonsRoute from './views/APH_RapprochementManuel/APH_RM_Doublons/RM_DoublonsRoutes'
import VER_VersementRoutes from './views/APH_RapprochementManuel/APH_VER_VersementEnRoute/VER_VersementRoutes'
import RM_IncoherenceRoute from './views/APH_Client_Transactions/ClientTransactionsRoutes'
import HistoriqueRoute from './views/APH_Clients/ClientInfoRoutes'
import dataToExcelRoute from './views/APH_DataToExcel/dataToExcelRoute'
import CJ_FerieRouter from './views/APH_JoursFerie/CJ_FerieRouter'
import RH_SuspendusRoute from './views/APH_RapprochementSuspendus/RH_SuspendusRoutes'
import UsersRoute from './views/APH_Users/UsersRoutes'
import PartenairesRoute from './views/APH_Partenaires/PartenairesRoutes'
import RM_AnnulationRoute from './views/APH_RapprochementManuel/APH_RM_Annulation/RM_AnnulationRoutes'
import ClientInfoRoute from './views/APH_Clients/ClientInfoRoutes'
const redirectRoute = [
    {
        path: '/',
        exact: true,
        component: () => <Redirect to="/Client-Transactions" />,
    },
]

const errorRoute = [
    {
        component: () => <Redirect to="/session/404" />,
    },
]

const routes = [
    ...dashboardRoutes,
    ...materialRoutes, 
    ...utilitiesRoutes,
    ...chartsRoute,
    ...dragAndDropRoute,
    ...calendarRoutes,
    ...invoiceRoutes,
    ...crudRoute,
    ...monitoringRoute,
    ...RM_DoublonsRoute,
    ...RM_IncoherenceRoute,
    ...RH_SuspendusRoute,
    ...RM_AnnulationRoute,
    ...HistoriqueRoute,
    ...VER_VersementRoutes,
    ...CJ_FerieRouter,
    ...UsersRoute,
    ...dataToExcelRoute,
    ...PartenairesRoute,
    ...inboxRoute,
    ...formsRoutes,
    ...mapRoutes,
    ...chatRoutes,
    ...todoRoutes,
    ...scrumBoardRoutes,
    ...ecommerceRoutes,
    ...pageLayoutRoutes,
    ...pricingRoutes,
    ...ListRoute,
    ...pagesRoutes,
    ...dataTableRoutes,
    ...redirectRoute,
    ...errorRoute,
    ...ClientInfoRoute,
    

]

export default routes
