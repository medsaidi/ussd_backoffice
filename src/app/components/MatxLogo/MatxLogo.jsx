import React from 'react'
import useSettings from 'app/hooks/useSettings'

const MatxLogo = ({ className }) => {
    const { settings } = useSettings()
    const theme = settings.themes[settings.activeTheme]

    return (
        <img
        className="w-200"
        src="/assets/images/advans/Advans_Tunisie_Logo.png"
        alt=""
    />
    )
}

export default MatxLogo
