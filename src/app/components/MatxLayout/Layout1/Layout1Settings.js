const Layout1Settings = {
    leftSidebar: {
        show: true,
        mode: 'full', // full, close, compact, mobile,
        theme: 'AdvansTheme', // View all valid theme colors inside MatxTheme/themeColors.js
        bgImgURL: '/assets/images/advans/perso-logo.png',
    },
    topbar: {
        show: true,
        fixed: true,
        theme: 'whiteBlue', // View all valid theme colors inside MatxTheme/themeColors.js
    },
}

export default Layout1Settings
