import axios from 'axios.js'

export const getAllUser = () => {
    return axios.post('/api/Historique')
}
export const getUserById = (id) => {
    return axios.get('/api/user', { data: id })
}
export const deleteUser = (User) => {
    return axios.post('/api/user/delete', User)
}
export const addNewUser = (User) => {
    return axios.post('/api/user/add', User)
}
export const updateUser = (User) => {
    return axios.post('/api/user/update', User)



}

export const getAllWithFiltre = (date,date_fin,part) => {
    return axios.post('/api/Historique',{hdR_DateOperation : date , hdR_DateOperation_Fin : date_fin ,  hdR_CodePartenaire : part })
}
