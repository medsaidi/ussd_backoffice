import axios from 'axios.js'

export const getAllWithFiltre = (fromDate, toDate) => {
    return axios.post('/api/Client/TransactionsClient', {
        FromDate: fromDate,
        ToDate: toDate,
    })
}
export const exportTransactionsClients = (fromDate, toDate) => {
    axios({
        method: 'post',
        url: '/api/Client/ExportTransactionsClients',
        responseType: 'blob',
        data: {
            FromDate: fromDate,
            ToDate: toDate,
        },
    })
        .then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]))
            const link = document.createElement('a')
            link.href = url
            link.setAttribute('download', 'ListTransactionsClients.xlsx')
            document.body.appendChild(link)
            link.click()
        })
        .catch((error) => console.log(error))
}
