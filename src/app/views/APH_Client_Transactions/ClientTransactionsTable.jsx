import React, { useState, useEffect } from 'react'
import { Button, Grid, Typography } from '@material-ui/core'
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers'

import DateFnsUtils from '@date-io/date-fns'
import { format } from 'date-fns'
import {
    getAllWithFiltre,
    exportTransactionsClients,
} from './ClientTransactionsServices'
import { Breadcrumb, SimpleCard } from 'app/components'
import MUIDataTable from 'mui-datatables'

const ClientTransactions = () => {
    const [transactionsList, setTransactionsList] = useState([])
    useEffect(() => {
        getAllWithFiltre(fromDate, toDate).then(({ data }) => {
            setTransactionsList(data)
        })
        // eslint-disable-next-line no-use-before-define
    }, [fromDate, toDate])
    const [toDate, setToDate] = useState(format(new Date(), 'yyyy-MM-dd'))
    const [fromDate, setFromDate] = useState(format(new Date(), 'yyyy-MM-dd'))

    const getByDate = () => {
        getAllWithFiltre(fromDate, toDate).then(({ data }) => {
            setTransactionsList(data)
        })
    }

    const onFromDateChange = (_date, value) => {
        setFromDate(value)
        localStorage.setItem('FromDate', JSON.stringify(value))
    }
    const onToDateChange = (_date, value) => {
        setToDate(value)
        localStorage.setItem('ToDate', JSON.stringify(value))
    }

    useEffect(() => {
        localStorage.removeItem('FromDate')
        localStorage.removeItem('ToDate')
    }, [])
    const columns = [
        {
            name: 'bankId',
            label: 'BankId',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'telcoId',
            label: 'TelcoId',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'amount',
            label: 'Amount',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'fee',
            label: 'Fee',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'status',
            label: 'Status',
            options: {
                filter: true,
                sort: true,
                customBodyRenderLite: (dataIndex) => {
                    const value = transactionsList[dataIndex]
                    const text = value.status ? 'Success' : 'Failure'
                    return (
                        <Typography
                            className="m-2"
                            variant="body1"
                            color="textPrimary"
                        >
                            {text}
                        </Typography>
                    )
                },
            },
        },
        {
            name: 'reason',
            label: 'Reason',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'transactionType',
            label: 'Transaction type',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'clientName',
            label: 'Client Name',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'originAccountNumber',
            label: 'Origin Account Number',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'originAccountNumber',
            label: 'Destination Account Number',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'branch',
            label: 'Branch',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'date',
            label: 'Date',
            options: {
                filter: true,
                viewColumns: true,

                customBodyRender: (value) =>
                    new Date(value).toLocaleDateString(),
            },
        },
        {
            name: 'date',
            label: 'Timestamp',
            options: {
                filter: true,
                sort: true,
                customBodyRender: (value) =>
                    new Date(value).toLocaleTimeString(),
            },
        },
        {
            name: 'network',
            label: 'Network',
            options: {
                filter: true,
                sort: true,
            },
        },
    ]
    return (
        <div className="m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb routeSegments={[{ name: 'Transactions' }]} />
            </div>

            <SimpleCard title="Critères de recherche">
                <Grid container spacing={3} alignItems="center">
                    <Grid item md={10} sm={8} xs={9}>
                        <div className="flex flex-wrap m--2">
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    className="m-2"
                                    margin="none"
                                    label="Date"
                                    inputVariant="outlined"
                                    type="text"
                                    size="small"
                                    autoOk={true}
                                    format="yyyy-MM-dd"
                                    inputValue={fromDate}
                                    onChange={onFromDateChange}
                                />
                            </MuiPickersUtilsProvider>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    className="m-2"
                                    margin="none"
                                    label="Date"
                                    inputVariant="outlined"
                                    type="text"
                                    size="small"
                                    autoOk={true}
                                    format="yyyy-MM-dd"
                                    inputValue={toDate}
                                    onChange={onToDateChange}
                                />
                            </MuiPickersUtilsProvider>

                            <Button
                                className="m-2 bg-green text-white"
                                variant="contained"
                                onClick={() => getByDate()}
                            >
                                Chercher
                            </Button>

                            <Button
                                className="m-2 bg-green text-white"
                                variant="contained"
                                onClick={() =>
                                    exportTransactionsClients(fromDate, toDate)
                                }
                            >
                                Export
                            </Button>
                        </div>
                    </Grid>
                </Grid>
            </SimpleCard>
            <br />

            <MUIDataTable
                title={'Transactions'}
                data={transactionsList}
                columns={columns}
                options={{
                    filterType: 'textField',
                    responsive: 'simple',
                    selectableRows: 'none',
                    fixedHeader: true,
                    pagination: true,
                    viewColumns: false,
                    elevation: 0,
                    rowsPerPageOptions: [10, 20, 40, 80, 100],
                }}
            />

            <br />
        </div>
    )
}

export default ClientTransactions
