import React from 'react'
import { authRoles } from '../../auth/authRoles'

const ClientTransactionsRoute = [
    {
        path: '/Client-Transactions',
        exact: true,
        component: React.lazy(() => import('./ClientTransactionsTable')),
        auth: authRoles.sa,
    },
]

export default ClientTransactionsRoute
