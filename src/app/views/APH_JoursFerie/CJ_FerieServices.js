import axios from 'axios.js'

export const getAllUser = (date) => {
    return axios.post('/api/Ferie' ,{
        cjF_DateFerie : date })
}
export const getUserById = (id) => {
    return axios.get('/api/user', { data: id })
}
export const ajouterDate = (date,desc) => {
    return axios.post('/api/Ferie/CreateJoursFerie',{
        cjF_DateFerie:date,
        cjF_Description : desc,

    } )
}
 
export const updateDate = (id) => {
    return axios.put('/api/Ferie' ,{
        cjF_ID : id })
}



export const EnvoiAmplitude = () => {
    return axios.post('/api/monitoring')
}