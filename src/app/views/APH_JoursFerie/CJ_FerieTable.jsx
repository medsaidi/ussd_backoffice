import React, { useState, useEffect } from 'react'
import {
    IconButton,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Icon,
    TablePagination,
    Button,
    Card,
    Grid,
 
} from '@material-ui/core'
import { getAllUser , updateDate } from './CJ_FerieServices'
import { Link } from 'react-router-dom'
import { Breadcrumb, ConfirmationDialog ,SimpleCard } from 'app/components'
import shortid from 'shortid'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
 
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import { format } from 'date-fns';
const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& thead': {
            '& th:first-child': {
                paddingLeft: 16,
            },
        },
        '& td': {
            borderBottom: 'none',
        },
        '& td:first-child': {
            paddingLeft: '16px !important',
        },
    },
}))

const CJ_FerieTable = () => {
    const [openDialog, setOpenDialog] = React.useState(false)
    const [rowsPerPage, setRowsPerPage] = useState(10)
    const [page, setPage] = useState(0)
    const [user, setUser] = useState(null)
    const [userList, setUserList] = useState([])
    const [selectedDate, setDate] = useState(format(new Date(),'yyyy-MM-dd')); 
    const [inputValue, setInputValue] = useState(format(new Date(),'yyyy-MM-dd')); 

    const classes = useStyles()

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }


    const onDateChange = (date, value) => {
        setDate(date);
        setInputValue(value);
        localStorage.setItem('DateSelectedFerie',JSON.stringify(value))
      };

    const handleDeleteUser = () => {
        const id=JSON.parse(localStorage.getItem('id_ferie')); 
        updateDate(id).then((response) => {
            console.log(response)
            updatePageData();
            handleClose();
        })
    }


    const handleOpen =(id) => {setOpenDialog(true) ; localStorage.setItem('id_ferie',id)}
 const handleClose =() => {setOpenDialog(false) ; localStorage.removeItem('id_ferie')}


    const updatePageData = () => {


        const date=JSON.parse(localStorage.getItem('DateSelectedFerie')); 
        console.log(date); 
       
          if (date != null)
      {
             setInputValue(date);
             getAllUser(date).then(({ data }) => {
              setUserList(data)

           
          })
       }
          else  {
            
            getAllUser(inputValue).then(({ data }) => {
                  setUserList(data)

                
              })
          }

    }

    useEffect(() => {
        updatePageData();
        localStorage.removeItem('DateSelectedFerie')
    }, [])

    return (
        <div className="m-sm-30">








<Dialog
          open={openDialog}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          disableBackdropClick
        >
          <DialogTitle id="alert-dialog-title">{"Confirmation"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
             Voulez vous Confirmer l'instruction !
            </DialogContentText>
          </DialogContent>
          <DialogActions>
        
            <Button onClick={handleClose}   className="m-2 bg-secondary text-white"
                            // color="primary"
                            variant="contained">
              Annuler
            </Button>
            <Button onClick={handleDeleteUser}   className="m-2 bg-green text-white"
                            // color="primary"
                            variant="contained" >
              Valider
            </Button>
          </DialogActions>
        </Dialog>


            <div className="mb-sm-30">
                <Breadcrumb routeSegments={[{ name: 'Jours Férié' }]} />
            </div>



            <Link to="/CJ-FerieForm">
            <Button
                className="mb-4"
                variant="contained"
                color="primary"
                 
            >
                Ajouter Une Date
            </Button>
<br/>




 

            </Link>


            <SimpleCard title="Critères de recherche">
                <Grid container spacing={3} alignItems="center">

                <Grid item md={10} sm={8} xs={12}>
                                    <div className="flex flex-wrap m--2">
                                        <MuiPickersUtilsProvider
                                            utils={DateFnsUtils}
                                        >
                                            <KeyboardDatePicker
                                                className="m-2"
                                                margin="none"
                                                label="Date"
                                                inputVariant="outlined"
                                                type="text"
                                                size="small"
                                                 autoOk={true}
                                                format="yyyy-MM-dd"
                                                // placeholder="format(new Date(),'yyyy-MM-dd')"
                                                 inputValue={inputValue}
                                                onChange={onDateChange}
                                            />
                                        </MuiPickersUtilsProvider>


                                        <Button
                                        className="m-2 bg-green text-white"
                                         variant="contained"
                                        //  color="bg-green"
                                         onClick={() => updatePageData()}
                                         >
                                          Chrercher
                                        </Button>
                                         
                                    </div>
                                </Grid>

                         
                            </Grid>                        
</SimpleCard>
<br/>
            <Card className="w-full overflow-auto" elevation={6}>
                <Table
                    className={clsx(
                        'whitespace-pre min-w-750',
                        classes.productTable
                    )}
                >
                    <TableHead>
                        <TableRow>
                            <TableCell>Date Férié</TableCell>
                            <TableCell>Description</TableCell>
                            <TableCell>Status</TableCell>
                            <TableCell>Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {userList
                            ?.slice(
                                page * rowsPerPage,
                                page * rowsPerPage + rowsPerPage
                            )
                            .map((user, index) => (
                                <TableRow hover key={shortid.generate()}>
                                    <TableCell className="px-0" align="left">
                                        {new Date (user.cjF_DateFerie).toLocaleDateString()}
                                    </TableCell>
                                    <TableCell className="px-0" align="left">
                                        {user.cjF_Description}
                                    </TableCell>
                                    
                                     
                                    <TableCell className="px-0">
                                        {user.cjF_Status ? (
                                            <small className="rounded bg-primary elevation-z3 text-white px-2 py-2px">
                                                active
                                            </small>
                                        ) : (
                                            <small className="rounded bg-error elevation-z3 px-2 py-2px">
                                                Closed
                                            </small>
                                        )}
                                    </TableCell>
                                    <TableCell className="px-0 border-none">
                                     
                                        <IconButton
                                            onClick={() =>
                                                handleOpen(user.cjF_ID)
                                                  
                                            }
                                        >
                                            <Icon color="error">delete</Icon>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                    </TableBody>
                </Table>

                <TablePagination
                    className="px-4"
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={userList?.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Next Page',
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={({ target: { value } }) =>
                        setRowsPerPage(value)
                    }
                />

               
            </Card>
        </div>
    )
}

export default CJ_FerieTable
