import React, { useState, useEffect, setState } from 'react'
import {ajouterDate,updateDate } from './CJ_FerieServices'
import {
    Grid,
    FormControl,
    RadioGroup,
    FormControlLabel,
    Radio,
    Card,
    Divider,
    TextField,
    Button,
    Backdrop

} from '@material-ui/core'
import { Breadcrumb } from 'app/components'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Slide from '@material-ui/core/Slide'
import { Link, useHistory } from 'react-router-dom'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    DatePicker,
} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import { format } from 'date-fns';
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />
})




const useStyles = makeStyles((theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            color: '#fff',
        },
    }),
);




const CJ_FerieForm = () => {
 




//snackbar pour date //
    const [rdS_Decision_updated, setdec] = useState(null)
    const [openDialog, setOpenDialog] = React.useState(false)
    const [openSnack, setOpenSnack] = React.useState(false);
    const [openSnackDec, setOpenSnackDec] = React.useState(false);
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [Comm, SetComm] = React.useState('');
    const [clearedDate, handleClearedDateChange] = useState(null);
    const history = useHistory() ;
    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    const handleCloseSnak = () => {
        setOpenSnack(false);
    };

    const handleCloseSnakDec = () => {
        setOpenSnackDec(false);
    };



    const handleChange = (event) => {

         SetComm(event.target.value)

    }
  

    const handleFormSubmitted = () => {
         if (Comm == null || Comm=='') { setOpenSnackDec(true); }
            else if(clearedDate ==null){setOpenSnack(true); }
         else {
            setOpen(true);
            ajouterDate(clearedDate,Comm).then((response) => {
                console.log(response)
                setOpen(false);
                setOpenSnack(true);
                history.push('/ferie-table')
            })
         }

    }

    function disableWeekends(date) {
        return date.getDay() === 0 || date.getDay() === 6;
      }
   
    useEffect(() => {

       
    }, [])
    return (
        <div className="m-sm-30">
            <Snackbar open={openSnack} autoHideDuration={5000} onClose={handleCloseSnak}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnak} severity="success" sx={{ width: '100%' }}>
                Date Ajouter avec success !
                </Alert>
            </Snackbar>


            <Snackbar open={openSnackDec} autoHideDuration={5000} onClose={handleCloseSnakDec}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnakDec} severity="error" sx={{ width: '100%' }}>
                    Description Obligatoire !
                </Alert>
            </Snackbar>


            <Backdrop className={classes.backdrop} open={open}>
                <CircularProgress color="inherit" />
            </Backdrop>


            <Dialog
                open={openDialog}
                TransitionComponent={Transition}
                disableBackdropClick
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">
                    {"Terminé"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                       Date Ajouter avec success !!
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Link to="/RH-Suspendus-table">
                        <Button className="m-2 bg-secondary text-white" >
                            Valider !
                        </Button>
                    </Link>

                </DialogActions>
            </Dialog>



            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: 'Jours Férié', path: '/ferie-table' },
                        { name: 'Ajout Date Frie' },
                    ]}
                />
            </div>


            <Card elevation={3}>
                <div className="flex p-4">
                    <h4 className="m-0">Edit Jours Férié</h4>
                </div>
                <Divider className="mb-2" />

                <div className="p-4" >
                    {/* <form className="p-4" onSubmit={handleFormSubmitted} > */}
                    <Grid container spacing={3} alignItems="center">
                        <Grid item md={12} sm={3} xs={6}>
                            Date férié
                        </Grid>
                        <Grid item md={12} sm={3} xs={6}>

                                        <MuiPickersUtilsProvider
                                            utils={DateFnsUtils}
                                        >
                                            

                                            <KeyboardDatePicker
                                             InputProps={{ readOnly: true }}
                                            clearable
                                            className="m-2"
                                            margin="none"
                                            label="Date"
                                            inputVariant="outlined"
                                            type="text"
                                            size="small"
                                            value={clearedDate}
                                            format="yyyy-MM-dd"
                                            onChange={handleClearedDateChange}
                                            shouldDisableDate={disableWeekends}
                                             />
                                        </MuiPickersUtilsProvider>


                                       
                        </Grid>

                        
                        <Grid item md={4} sm={4} xs={6}>
                            Description
                        </Grid>
                        <Grid item md={12} sm={8} xs={6}>
                            <TextField
                                label="Description"
                                multiline
                                name="Comm"
                                value={Comm}
                                rows={4}
                                onChange={handleChange}
                                //   style={{ width: 500 }} 
                                fullWidth
                                variant="outlined"
                                required
                            />
                        </Grid>
                    </Grid>


                    <div className="mt-6">
                        <Button
                            className="m-2 bg-green text-white"
                            // color="primary"
                            variant="contained"
                            //type="submit"
                            // onSubmit={handleFormSubmitted()}
                            onClick={handleFormSubmitted}
                        //  onClick={() => {

                        //     handleFormSubmitted();
                        //     // handleClose();
                        //   }}
                        >
                            Enregistrer 
                        </Button>
                        <Link to="/ferie-table">
                            <Button
                                className="m-2 bg-secondary text-white"
                                variant="contained"
                                type="submit"

                            >
                                Annuler
                            </Button>

                        </Link>
                    </div>
                </div>


            </Card>
        </div>
    )
}

export default CJ_FerieForm
