import React from 'react'
import { authRoles } from '../../auth/authRoles'

const CJ_FerieRouter = [
    {
        path: '/ferie-table',
        exact: true,
        component: React.lazy(() => import('./CJ_FerieTable')),
        auth: authRoles.sa,
    },
    {
        path: '/CJ-FerieForm',
        component: React.lazy(() =>
            import('./CJ_FerieForm')
        ),
    },
]

export default CJ_FerieRouter
