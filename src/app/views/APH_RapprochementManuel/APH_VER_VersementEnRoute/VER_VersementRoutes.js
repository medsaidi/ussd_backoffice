import React from 'react'
import { authRoles } from '../../../auth/authRoles'

const VER_VersementRoutes = [
    {
        path: '/VER-Versement-table',
        exact: true,
        component: React.lazy(() => import('./VER_VersementTable')),
        auth: authRoles.sa,
    },
    // {
    //     path: '/RM-DoublonsForm',
    //     component: React.lazy(() =>
    //         import('./VER_VersementForm')
    //     ),
    // },
]

export default VER_VersementRoutes
