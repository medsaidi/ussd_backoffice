import React, { useState, useEffect } from 'react'
import axios from 'axios.js'
import {
    IconButton,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Icon,
    TablePagination,
    Button,
    Card,
    TextField,
    Grid,
    MenuItem,
    Backdrop
} from '@material-ui/core'
import {getAllWithFiltre ,UpdatedArray} from './VER_VersementServices'
import { Breadcrumb, ConfirmationDialog,SimpleCard } from 'app/components'
import { makeStyles,createStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import { format } from 'date-fns';
import CircularProgress from '@material-ui/core/CircularProgress'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Slide from '@material-ui/core/Slide'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />
})

const useStyless = makeStyles((theme) =>
createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
  }),
);


const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& thead': {
            '& th:first-child': {
                paddingLeft: 16,
            },
        },
        '& td': {
            borderBottom: 'none',
        },
        '& td:first-child': {
            paddingLeft: '16px !important',
        },
    },


  
}))

const VER_VersementTable = () => {
     
    const [rowsPerPage, setRowsPerPage] = useState(10)
    const [page, setPage] = useState(0)
    const [user, setUser] = useState(null)
    const [userList, setUserList] = useState([])
    const [itemlist, setItemList] = useState([])
    const [selected, setSelected] = useState('');
    const [selectedDec, setSelectedDec] = useState(null);
    const [selectedDate, setDate] = useState(format(new Date(),'yyyy-MM-dd')); 
    const [inputValue, setInputValue] = useState(format(new Date(),'yyyy-MM-dd')); 
    const classe = useStyless()
    const classes = useStyles()
    
    const [open, setOpen] = React.useState(false);
    const [openAlert, setOpenAlert] = React.useState(false);
    const [openSnack, setOpenSnack] = React.useState(false);
    const [openSnackError, setOpenSnackError] = React.useState(false);
 
    const handleClose = () => {
        setOpenAlert(false);
    };






    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }


    const onDateChange = (date, value) => {
        setDate(date);
        setInputValue(value);
        localStorage.setItem('DateSelectedVersement',JSON.stringify(value))
        localStorage.setItem('TestForDateVersement','oui')
      };
    

      const  handleChangeDec = (event , id)=> {
        setSelectedDec(event.target.value); 
        var users = localStorage.getItem('userConnected') ;
        const Versement = { veR_ID : 0, veR_Decision : '' , veR_UserModified : users}
        Versement.veR_ID=id ;
        Versement.veR_Decision=event.target.value ;
        var i=0 ; 
        var ok=false ; 
        while(itemlist.length>i && ok==false)
        {
            if(itemlist[i].veR_ID == id && Versement.veR_Decision !=null )
            {
                itemlist[i].veR_Decision=event.target.value;
                ok=true ;
            }
            else if(itemlist[i].veR_ID == id && Versement.veR_Decision ==null)
            {
                itemlist.splice(i, 1);
                ok=true ; 
            }
            else{
                i++ ;
            }

        }
        if(ok==false){itemlist.push(Versement)}
console.log(itemlist)
// localStorage.setItem("listed", JSON.stringify(itemlist))
        }
    const  handleChange = (event)=> {
    setSelected(event.target.value);  
   
    }

 

    const getbydate = () =>{

        
        const date=JSON.parse(localStorage.getItem('DateSelectedVersement')); 
        const test=localStorage.getItem('TestForDateVersement');
        if(date!=null && date.includes('_')){setOpenAlert(true)}
       else{
          if (date != null)
          {
             setInputValue(date);
             getAllWithFiltre(date,selected).then(({ data }) => {
              setUserList(data)
           })
          }
          else if(date==null && test=='oui') {
            
              getAllWithFiltre(date,selected).then(({ data }) => {
                  setUserList(data);
                 
                
            }) 
            
         }

         else{
            getAllWithFiltre(inputValue,selected).then(({ data }) => {
                setUserList(data) ;
                
            }) 
          }
          
        }
    }

    const handleFormSubmitted = () => {
        if(itemlist.length==0){setOpenSnackError(true)}
        else{
        setOpen(true);
        const lis=JSON.parse(localStorage.getItem('listed')); 
        axios.put('/api/VersementEnRoute',itemlist) .then( (response) =>{
            itemlist.splice(0,itemlist.length)
            getbydate();
            setOpen(false);
            setOpenSnack(true);
              // setSelectedDec(null);
          })
        }
         
    }


    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
      }

      const handleCloseSnak = () => {
        setOpenSnack(false);
      };

      const handleCloseSnakError = () => {
        setOpenSnackError(false);
      };

    useEffect(() => {
        getbydate()
         localStorage.removeItem('DateSelected')
         localStorage.removeItem('TestForDateVersement')
    }, [])
    return (
        <div className="m-sm-30">

<Snackbar open={openSnack} autoHideDuration={6000} onClose={handleCloseSnakError}
 anchorOrigin={{
    vertical: 'top',
    horizontal: 'center',
}}>
        <Alert onClose={handleCloseSnak} severity="success" sx={{ width: '100%' }}>
         Traitement terminé avec succées !
        </Alert>
      </Snackbar>


      <Snackbar open={openSnackError} autoHideDuration={6000} onClose={handleCloseSnakError}
 anchorOrigin={{
    vertical: 'top',
    horizontal: 'center',
}}>
        <Alert onClose={handleCloseSnakError} severity="error" sx={{ width: '100%' }}>
         Vous devez Choisir Au Moins Une Ligne !
        </Alert>
      </Snackbar>


             <Backdrop className={classe.backdrop} open={open}>
        <CircularProgress color="inherit" />
      </Backdrop>
 
           
      <Dialog
        open={openAlert}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Invalid Date"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Veillez Saisir Une Date Valide
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} autoFocus>
            Fermer
          </Button>
        </DialogActions>
      </Dialog>











     
            <div className="mb-sm-30">
                <Breadcrumb routeSegments={[{ name: 'Versement en route' }]} />
            </div>
            <SimpleCard title="Critères de recherche">
                <Grid container spacing={3} alignItems="center">

                <Grid item md={10} sm={8} xs={12}>
                                    <div className="flex flex-wrap m--2">
                                    <MuiPickersUtilsProvider
                                            utils={DateFnsUtils}
                                        >
                                            <KeyboardDatePicker
                                                className="m-2"
                                                margin="none"
                                                label="Date"
                                                inputVariant="outlined"
                                                type="text"
                                                size="small"
                                                 autoOk={true}
                                                format="yyyy-MM-dd"
                                                // placeholder="format(new Date(),'yyyy-MM-dd')"
                                                  inputValue={inputValue}
                                                onChange={onDateChange}
                                               
                                            />
                                        </MuiPickersUtilsProvider>
                                         

                                        <TextField
                                            className="m-2 min-w-188"
                                            label="Partenaire"
                                            name="part"
                                            size="small"
                                            variant="outlined"
                                            
                                            value={selected}
                                            onChange={handleChange}
                                            select
                                        >
                                            {paymentTermList.map(
                                                (item, ind) => (
                                                    <MenuItem
                                                        value={item.value}
                                                        key={item.id}
                                                    >
                                                        {item.content}
                                                    </MenuItem>
                                                )
                                            )}
                                        </TextField>

                                        <Button
                                        className="m-2 bg-green text-white"
                                         variant="contained"
                                         onClick={() => getbydate()}
                                         >
                                          Chercher
                                        </Button>
                                         
                                    </div>
                                </Grid>

                         
                            </Grid>                        



            </SimpleCard>
            <br />
            <Card className="w-full overflow-auto" elevation={6}>
                <Table
                    className={clsx(
                        'whitespace-pre min-w-750',
                        classes.productTable
                    )}
                >
                    <TableHead>
                        <TableRow>
                            <TableCell>Date Op</TableCell>
                            <TableCell>Libellé</TableCell>
                            <TableCell>Montant débit</TableCell>
                            <TableCell>Montant drédit</TableCell>
                            <TableCell>Décision</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {userList
                            ?.slice(
                                page * rowsPerPage,
                                page * rowsPerPage + rowsPerPage
                            )
                            .map((user, index) => (
                                <TableRow hover key={user.veR_ID}>
                                      <TableCell className="px-0" align="left">
                                      {new Date (user.veR_DateTransaction).toLocaleDateString()}
                                    </TableCell>
                                     <TableCell className="px-0" align="left">
                                    {user.veR_Libelle}
                                    </TableCell>
                                     <TableCell className="px-0" align="left">
                                        {user.veR_Debit}
                                    </TableCell>
                                    <TableCell className="px-0" align="left">
                                        {user.veR_Credit}
                                    </TableCell>
                                    
                                    <TableCell className="px-0 border-none">
                                    {<TextField 
                                         className="m-2 min-w-188"
                                        label="Décision"
                                        id='Selection'
                                        name="Décision"

                                        size="small"
                                        variant="outlined"
                                        select
                                      //value={selectedDec}
                                      onChange={e => {handleChangeDec(e ,user.veR_ID );
                                       
                                        
                                     }}
                                        variant="outlined"

                                    >
                                        {DecisionList.map((item, ind) => (
                                            <MenuItem
                                                value={item.value}
                                                key={item.id}                                           
                                            >
                                                {item.content}    
                                            </MenuItem>
                                        ))}


                                    </TextField>}
                                    </TableCell>
                                </TableRow>
                            ))}
                    </TableBody>
                </Table>

       
                <TablePagination
                    className="px-4"
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={userList?.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Next Page',
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={({ target: { value } }) =>
                        setRowsPerPage(value)
                    }
                />
            </Card>

            <div className="mt-6">
                                <Button
                                    className="m-2 bg-green text-white"
                                    // color="primary"
                                    variant="contained"
                                    type="submit"
                                    onClick={handleFormSubmitted}
                                      //onClick={}
                                    //  onClick={() => {
                                         
                                    //     handleFormSubmitted();
                                    //     // handleClose();
                                    //   }}
                                >
                                    Enregistrer
                                </Button>
                                 
                                <Button
                                    className="m-2 bg-secondary text-white"
                                    variant="contained"
                                    type="submit"
                                    
                                >
                                    Annuler
                                </Button>

                               
                            </div>

        </div>
    )
}
const paymentTermList = [
    {id :0 , title:'' ,content :'Tous'  , value: ''},
    {id :1 ,title:'', content :'AMEN' , value :'AMEN'},
    {id :2,title:'', content :'AWB' , value :'AWB'},
]
const DecisionList = [
    {id :0 , title:'' ,content :'Sélectionner'  , value:null},
    {id :1 ,title:'', content :'Ignorer' , value :'IGNORED'},
     {id :2,title:'', content :'Suspendre' , value :'SUSPENDED'},

]

export default VER_VersementTable
