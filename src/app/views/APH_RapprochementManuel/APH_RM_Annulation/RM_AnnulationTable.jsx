import React, { useState, useEffect } from 'react'
import axios from 'axios.js'
import {
    IconButton,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Icon,
    TablePagination,
    Button,
    Card,
    TextField,
    MenuItem,
    Backdrop,
    Grid
} from '@material-ui/core'
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import { format } from 'date-fns';
import { getAllWithFiltre, deleteUser, updateannulation } from './RM_AnnulationServices'
import MemberEditorDialog from './RM_AnnulationEditorDialog'
import { Breadcrumb, ConfirmationDialog,SimpleCard } from 'app/components'
import shortid from 'shortid'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import CircularProgress from '@material-ui/core/CircularProgress'


const useStyless = makeStyles((theme) =>
createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
  }),
);


const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& thead': {
            '& th:first-child': {
                paddingLeft: 16,
            },
        },
        '& td': {
            borderBottom: 'none',
        },
        '& td:first-child': {
            paddingLeft: '16px !important',
        },
    },
}))







const RM_AnnulationTable = () => {
 
    const [rowsPerPage, setRowsPerPage] = useState(10)
    const [page, setPage] = useState(0)
    const [user, setUser] = useState(null)
    const [userList, setUserList] = useState([])
    const [selectedDate, setDate] = useState(format(new Date(),'yyyy-MM-dd')); 
    const [inputValue, setInputValue] = useState(format(new Date(),'yyyy-MM-dd')); 
    const classe = useStyless()
    const classes = useStyles()
    const [selected, setSelected] = useState('');
    const [open, setOpen] = React.useState(false);
    const [openSnackError, setOpenSnackError] = React.useState(false);
    const [openSnack, setOpenSnack] = React.useState(false);
    const [openAlert, setOpenAlert] = React.useState(false);
 
    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const getbydate = () =>{

        
        const date=JSON.parse(localStorage.getItem('DateSelectedAnnulation')); 
        const test=localStorage.getItem('TestForDateAnnulation');
         
          if (date != null)
          {
             setInputValue(date);
             getAllWithFiltre(date,selected).then(({ data }) => {
              setUserList(data)
           })
          }
          else if(date==null && test=='oui') {
            
              getAllWithFiltre(date,selected).then(({ data }) => {
                  setUserList(data);
                 
                
            }) 
            
         }

         else{
            getAllWithFiltre(inputValue,selected).then(({ data }) => {
                setUserList(data) ;
                
            }) 
          }
          
        
    }




    useEffect(() => {
        getbydate()
        localStorage.removeItem('DateSelectedAnnulation')
        localStorage.removeItem('TestForDateAnnulation')
    }, [])

    const  handleChange = (event)=> {
        setSelected(event.target.value);  
       
        }
        const onDateChange = (date, value) => {
            setDate(date);
            setInputValue(value);
            localStorage.setItem('DateSelectedAnnulation',JSON.stringify(value))
            localStorage.setItem('TestForDateAnnulation','oui')
          };
    const handleCloseSnak = () => {
        setOpenSnack(false);
      };

      const handleCloseSnakError = () => {
        setOpenSnackError(false);
      };

 
    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
      }
 

    const handleFormSubmitted = () => {

        if(itemlist.length==0){setOpenSnackError(true)}
        else{
        setOpen(true);
        axios.put('/api/Annulataion',itemlist) .then( (response) =>{
            itemlist.splice(0,itemlist.length)
            getbydate();
            setOpen(false);
            setOpenSnack(true);
              // setSelectedDec(null);
          })
        }

    }

    const [selectedDec, setSelectedDec] = useState(null);
    const [itemlist, setItemList] = useState([])
    const  handleChangeDec = (event , id)=> {
        setSelectedDec(event.target.value); 
        var users = localStorage.getItem('userConnected') ;
        const Annulation = { adT_ID : 0, veR_Decision : '' , adT_UserModified : users }
        Annulation.adT_ID=id ;
        Annulation.veR_Decision=event.target.value ;
        var i=0 ; 
        var ok=false ; 
        while(itemlist.length>i && ok==false)
        {
            if(itemlist[i].adT_ID == id && Annulation.veR_Decision !=null )
            {
                itemlist[i].veR_Decision=event.target.value;
                ok=true ;
            }
            else if(itemlist[i].adT_ID == id && Annulation.veR_Decision ==null)
            {
                itemlist.splice(i, 1);
                ok=true ; 
            }
            else{
                i++ ;
            }

        }
        if(ok==false){itemlist.push(Annulation)}
console.log(itemlist)
// localStorage.setItem("listed", JSON.stringify(itemlist))
        }

 

 



    return (
        <div className="m-sm-30">

<Snackbar open={openSnack} autoHideDuration={6000} onClose={handleCloseSnakError}
 anchorOrigin={{
    vertical: 'top',
    horizontal: 'center',
}}>
        <Alert onClose={handleCloseSnak} severity="success" sx={{ width: '100%' }}>
         Traitement terminé avec succées !
        </Alert>
      </Snackbar>


      <Snackbar open={openSnackError} autoHideDuration={6000} onClose={handleCloseSnakError}
 anchorOrigin={{
    vertical: 'top',
    horizontal: 'center',
}}>
        <Alert onClose={handleCloseSnakError} severity="error" sx={{ width: '100%' }}>
         Vous devez Choisir Au Moins Une Ligne !
        </Alert>
      </Snackbar>


             <Backdrop className={classe.backdrop} open={open}>
        <CircularProgress color="inherit" />
      </Backdrop>




            <div className="mb-sm-30">
                <Breadcrumb routeSegments={[{ name: 'Annulation' }]} />
            </div>
            <SimpleCard title="Critères de recherche">
                <Grid container spacing={3} alignItems="center">

                <Grid item md={10} sm={8} xs={12}>
                                    <div className="flex flex-wrap m--2">
                                    <MuiPickersUtilsProvider
                                            utils={DateFnsUtils}
                                        >
                                            <KeyboardDatePicker
                                                className="m-2"
                                                margin="none"
                                                label="Date"
                                                inputVariant="outlined"
                                                type="text"
                                                size="small"
                                                 autoOk={true}
                                                format="yyyy-MM-dd"
                                                // placeholder="format(new Date(),'yyyy-MM-dd')"
                                                  inputValue={inputValue}
                                                onChange={onDateChange}
                                               
                                            />
                                        </MuiPickersUtilsProvider>
                                         

                                        <TextField
                                            className="m-2 min-w-188"
                                            label="Partenaire"
                                            name="part"
                                            size="small"
                                            variant="outlined"
                                            
                                            value={selected}
                                            onChange={handleChange}
                                            select
                                        >
                                            {paymentTermList.map(
                                                (item, ind) => (
                                                    <MenuItem
                                                        value={item.value}
                                                        key={item.id}
                                                    >
                                                        {item.content}
                                                    </MenuItem>
                                                )
                                            )}
                                        </TextField>

                                        <Button
                                        className="m-2 bg-green text-white"
                                         variant="contained"
                                         onClick={() => getbydate()}
                                         >
                                          Chercher
                                        </Button>
                                         
                                    </div>
                                </Grid>

                         
                            </Grid>                        



            </SimpleCard>
            <br />





            <Card className="w-full overflow-auto" elevation={6}>
                <Table
                    className={clsx(
                        'whitespace-pre min-w-750',
                        classes.productTable
                    )}
                >
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" style={{ whiteSpace: 'unset', width: 200 }}>Partenaire</TableCell>
                            <TableCell align="center" style={{ whiteSpace: 'unset', width: 200 }}>Date Op</TableCell>
                            {/* <TableCell>CT</TableCell>
                            <TableCell>CIN</TableCell> */}
                            <TableCell align="center" style={{ whiteSpace: 'unset', width: 200 }}>Montant</TableCell>
                            <TableCell align="center" >Description</TableCell>
                            <TableCell align="center" >Action</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {userList
                            ?.slice(
                                page * rowsPerPage,
                                page * rowsPerPage + rowsPerPage
                            )
                            .map((user, index) => (
                                <TableRow hover key={user.adT_ID}>
                                    <TableCell className="px-0" align="center">
                                        {user.adT_CodePartenaire}
                                    </TableCell>
                                    <TableCell className="px-0" align="center">
                        {new Date(user.adT_DateOperation).toLocaleDateString()+ ' '+new Date(user.adT_DateOperation).toLocaleTimeString()}
                                    </TableCell>

 
                                    <TableCell className="px-0" align="center">

                                        {user.adT_MontantVerse}
                                    </TableCell>

                                    <TableCell className="px-0" align="center">
                                        {user.adT_Libelle}
                                    </TableCell>
                                    <TableCell className="px-0" align="center">
                                  
                                        {<TextField 
                                         className="m-2 min-w-188"
                                        label="Décision"
                                        id='Selection'
                                        name="Décision"

                                        size="small"
                                        variant="outlined"
                                        select
                                      //value={selectedDec}
                                      onChange={e => {handleChangeDec(e ,user.adT_ID );
                                       
                                        
                                     }}
                                        variant="outlined"

                                    >
                                        {DecisionList.map((item, ind) => (
                                            <MenuItem
                                                value={item.value}
                                                key={item.id}                                           
                                            >
                                                {item.content}    
                                            </MenuItem>
                                        ))}


                                    </TextField>}


                                    </TableCell>

                                </TableRow>
                            ))}
                    </TableBody>
                </Table>
                <div className="mt-6">
                                <Button
                                    className="m-2 bg-green text-white"
                                    // color="primary"
                                    variant="contained"
                                    type="submit"
                                    onClick={handleFormSubmitted}
                                      //onClick={}
                                    //  onClick={() => {
                                         
                                    //     handleFormSubmitted();
                                    //     // handleClose();
                                    //   }}
                                >
                                    Enregistrer
                                </Button>
                                 
                                <Button
                                    className="m-2 bg-secondary text-white"
                                    variant="contained"
                                    type="submit"
                                    
                                >
                                    Annuler
                                </Button>

                               
                            </div>

        
               
                <TablePagination
                    className="px-4"
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={userList?.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Next Page',
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={({ target: { value } }) =>
                        setRowsPerPage(value)
                    }
                />
 
              
            </Card>
            
        </div>
    )
}

const DecisionList = [
    {id :0 , title:'' ,content :'Sélectionner'  , value:null},
    {id :1 ,title:'', content :'Ignorer' , value :'IGNORED'},

]

const paymentTermList = [
    {id :0 , title:'' ,content :'Tous'  , value: ''},
    {id :1 ,title:'', content :'AMEN' , value :'AMEN'},
    {id :2,title:'', content :'AWB' , value :'AWB'},
]

export default RM_AnnulationTable
