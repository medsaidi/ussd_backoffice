import React from 'react'
import { authRoles } from '../../../auth/authRoles'

const RM_AnnulationRoute = [
    {
        path: '/RM-Annulation-table',
        exact: true,
        component: React.lazy(() => import('./RM_AnnulationTable')),
        auth: authRoles.sa,
    },
]

export default RM_AnnulationRoute
