import React, { useState, useEffect } from 'react'
import {
    IconButton,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Icon,
    TablePagination,
    Button,
    Card,
    TextField,
    MenuItem,
    Grid,
} from '@material-ui/core'
import DateFnsUtils from '@date-io/date-fns'
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers'
import { getAllUser ,getAllWithFiltre,VerifDoublon } from './RM_DoublonsServices'
import { Breadcrumb ,SimpleCard } from 'app/components'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import { format } from 'date-fns';
import {ExportToExcel} from 'ExportToExcel'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import MUIDataTable from 'mui-datatables'
const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& thead': {
            '& th:first-child': {
                paddingLeft: 16,
            },
        },
        '& td': {
            borderBottom: 'none',
        },
        '& td:first-child': {
            paddingLeft: '16px !important',
        },
    },
}))

const RM_DoublonsTable = () => {
    const [uid, setUid] = useState(null)
    const [rowsPerPage, setRowsPerPage] = useState(10)
    const [page, setPage] = useState(0)
    const [user, setUser] = useState(null)
    const [userList, setUserList] = useState([])
    const [openSnack, setOpenSnack] = React.useState(false);
    const [selected, setSelected] = useState('');
    const [selectedDate, setDate] = useState(format(new Date(),'yyyy-MM-dd')); 
    const [inputValue, setInputValue] = useState(format(new Date(),'yyyy-MM-dd')); 
    const fileName = "myfile";
    const [userListexcel, setUserListexcel] = useState([])

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }


    const handleCloseSnak = () => {
        setOpenSnack(false);
    };




    const classes = useStyles()

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }
 
    const onDateChange = (date, value) => {
        setDate(date);
        setInputValue(value);
        localStorage.setItem('DateSelectedDoublon',JSON.stringify(value))
      };
    


    const  handleChange = (event)=> {
    setSelected(event.target.value);  
    }
 


    const getbydate = () =>{

        
        const date=JSON.parse(localStorage.getItem('DateSelectedDoublon')); 
        console.log(date); 
       
          if (date != null)
      {
             setInputValue(date);
             VerifDoublon(date).then((res) =>{
                if(res.data)
                {
                    setOpenSnack(true);
                }
                else 
                {
                    getAllWithFiltre(date,selected).then(({ data }) => {
                        setUserList(data)
                        updatePageData();
                     
                    })
                }

             })
           
       }
          else  {
            VerifDoublon(inputValue).then((res) =>{
                if(res.data){ setOpenSnack(true);}
                else{
                    getAllWithFiltre(inputValue,selected).then(({ data }) => {
                        setUserList(data)
                        updatePageData();
                      
                    }) 

                }
            })
          }
                    //   getAllWithFiltre(inputValue,selected).then(({ data }) => {
                    //     setUserList(data)
                    //     updatePageData();
                      
                    //  }) 

                                             
            
    }


    const updatePageData = () => {
        getAllWithFiltre(inputValue,selected).then(( response ) => {
            let newArray = [];
            response.data.map((item) => {
                let obj = { CT: item.rmD_CT, CIN: item.rmD_CIN };
                newArray.push(obj);
                setUserListexcel(newArray)
        });
         
        }) 
    }
 
 
    useEffect(() => {
        
        getbydate()
        updatePageData();
         localStorage.removeItem('DateSelectedDoublon')
    }, [])

    return (
        <div className="m-sm-30">

            <Snackbar open={openSnack} autoHideDuration={6000} onClose={handleCloseSnak}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnak} severity="error" sx={{ width: '100%' }}>
                    Il faut traiter les incohérences de la date selectionnée !
                </Alert>
            </Snackbar>

            <div className="mb-sm-30">
                <Breadcrumb routeSegments={[{ name: 'Doublons' }]} />
            </div>
     

                                      
           
            <SimpleCard title="Critères de recherche">
                <Grid container spacing={3} alignItems="center">

                <Grid item md={10} sm={8} xs={12}>
                                    <div className="flex flex-wrap m--2">
                                        <MuiPickersUtilsProvider
                                            utils={DateFnsUtils}
                                        >
                                            <KeyboardDatePicker
                                                className="m-2"
                                                margin="none"
                                                label="Date"
                                                inputVariant="outlined"
                                                type="text"
                                                size="small"
                                                 autoOk={true}
                                                format="yyyy-MM-dd"
                                                // placeholder="format(new Date(),'yyyy-MM-dd')"
                                                 inputValue={inputValue}
                                                onChange={onDateChange}
                                            />
                                        </MuiPickersUtilsProvider>

                                        <TextField
                                            className="m-2 min-w-188"
                                            label="Partenaire"
                                            name="part"
                                            size="small"
                                            variant="outlined"
                                            
                                            value={selected}
                                            onChange={handleChange}
                                            select
                                        >
                                            {paymentTermList.map(
                                                (item, ind) => (
                                                    <MenuItem
                                                        value={item.value}
                                                        key={item.id}
                                                    >
                                                        {item.content}
                                                    </MenuItem>
                                                )
                                            )}
                                        </TextField>

                                        <Button
                                        className="m-2 bg-green text-white"
                                         variant="contained"
                                        //  color="bg-green"
                                         onClick={() => getbydate()}
                                         >
                                          Chercher
                                        </Button>
                                         
                                    </div>
                                </Grid>

                         
                            </Grid>                        



            </SimpleCard>
            <br />
            <Card className="w-full overflow-auto" elevation={6}>
              

                              
                                                    {/* // setUid(user.id)
                                                    // setShouldOpenEditorDialog(true)
                                                    localStorage.setItem("iD_doublons",user.rmD_ID)
                                                    localStorage.setItem("id_doublons_doub",user.rmD_ID_Doublons)
                                                    localStorage.setItem("SourceDoublons",user.rmD_Source) */}
        <MUIDataTable
                title={'Liste des doublons'}
                data={userList}
                 columns={columns}
                options={{
                    filterType: 'textField',
                    responsive: 'simple',
                    selectableRows: 'none',  
                    fixedHeader : true,
                    pagination: true,  
                    viewColumns: false , 
                    elevation: 0,
                    rowsPerPageOptions: [10, 20, 40, 80, 100],
                    onRowClick: (rowData) => {
                        console.log(rowData[0] , rowData[1],rowData[2]);
                         localStorage.setItem("iD_doublons",rowData[0])
                         localStorage.setItem("id_doublons_doub",rowData[1])
                         localStorage.setItem("SourceDoublons",rowData[2])
                        // localStorage.setItem('id_sus',rowData[0])
                        // localStorage.setItem('SourceSus',rowData[1])
                       
                     },
                    
                }}
                /> 

               
                
            </Card>
        </div>
    )
}


const paymentTermList = [
    {id :0 , title:'' ,content :'Tous'  , value: ''},
    {id :1 ,title:'', content :'AMEN' , value :'AMEN'},
    {id :2,title:'', content :'AWB' , value :'AWB'},
]



const columns = [
    {
        name: "rmD_ID",
        label: "Id",
        options: {
          display: false,
          filter: false,
          sort: false,
        }
      },
      {
        name: "rmD_ID_Doublons",
        label: "Id_doub",
        options: {
          display: false,
          filter: false,
          sort: false,
        }
      },
      {
        name: "rmD_Source",
        label: "Partenaire",
       
        options: {
            filter: true,
            
        }
        
      },
    {
        name: 'rmD_DateTransaction', // field name in the row object
        label: 'Date Transaction', // column title that will be shown in table
        options: {
            filter: true,
            customBodyRender: (value) => new Date(value).toLocaleDateString()+ ' '+new Date(value).toLocaleTimeString()
        },
    },
     
    {
        name: 'rmD_CT', // field name in the row object
        label: 'CT', // column title that will be shown in table
        options: {
            filter: true,
        },
    },
    {
        name: 'rmD_CIN', // field name in the row object
        label: 'CIN', // column title that will be shown in table
        options: {
            filter: true,
        },
    },
 
    {
        name: 'rmD_FullName', // field name in the row object
        label: 'Nom client', // column title that will be shown in table
        options: {
            filter: true,
        },
    },
    {
        name: "Action",
        options: {
          filter: false,
          sort: false,
          empty: true,
          customBodyRenderLite: () => {
              
            return (
             <Link to="/RM-DoublonsForm"> 
              <IconButton>
               <Icon color="primary">edit</Icon>
              </IconButton>
               </Link>
            );
          }
        }
      },
     
]



export default RM_DoublonsTable
