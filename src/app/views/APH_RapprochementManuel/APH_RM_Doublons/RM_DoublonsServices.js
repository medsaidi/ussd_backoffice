import axios from 'axios.js'
import { id } from 'date-fns/locale'


export const getAllWithFiltre = (date,part) => {
    return axios.post('/api/Doublons',{rmD_DateTransaction : date , rmD_Source : part })
}

export const getAllUser = () => {
    return axios.get('/api/Doublons')
}
export const getUserById = (id) => {
    return axios.get('/api/Doublons/'+id, { data: id })
}
export const deleteUser = (User) => {
    return axios.post('/api/user/delete', User)
}
export const addNewUser = (User) => {
    return axios.post('/api/user/add', User)
}
export const UpdatbyDoubons = (id,Dec,id_doub , dec_doub,source , comm_orig, comm_doub, user) => {
    return axios.put('/api/Doublons',{
        rmD_ID:id,
        rmD_D_Decision:dec_doub,
        rmD_ID_Doublons : id_doub,
        rmD_O_Decision :Dec,
        rmD_Source : source,
        rmD_O_Commentaire :comm_orig,
        rmD_D_Commentaire : comm_doub,
        rmD_UserModified :  user 
    })
}
export const updateUser = (id,Dec,cmt) => {
    return axios.put('/api/Doublons',{
        rmD_ID:id,
        rmD_Decision:Dec,
        rmD_Commentaire:cmt
    })
}

export const VerifDoublon = (date) => {
    return axios.post('/api/Doublons/verificationDoublon',{rmD_DateTransaction : date})
}

