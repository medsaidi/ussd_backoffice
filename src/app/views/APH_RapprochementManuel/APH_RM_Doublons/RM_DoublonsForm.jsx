import React, { useState, useEffect, setState } from 'react'
import { getUserById, getAllUser, UpdatbyDoubons } from './RM_DoublonsServices'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import CircularProgress from '@material-ui/core/CircularProgress'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import {
    Grid,
    FormControl,
    RadioGroup,
    FormControlLabel,
    Radio,
    Card,
    Divider,
    TextField,
    MenuItem,
    Tabs,
    Tab,
    Button,
    Select,
     Backdrop,

} from '@material-ui/core'
import { Breadcrumb } from 'app/components'
import { Link, useHistory } from 'react-router-dom'

const RM_DoublonsForm = () => {
    const selectList = [
        { id: 0, title: '', content: 'Selectionner', value: null },
        { id: 1, title: '', content: 'Valider', value: 'CORRECTED' },
        { id: 2, title: '', content: 'Suspendus', value: 'SUSPENDED' },
        { id: 3, title: '', content: 'Envoyée', value: 'SENDED' , hidden : true},
        { id: 4, title: '', content: 'Suspendus envoyée', value: 'SuspendedSENDED' , hidden : true},
        { id: 5, title: '', content: 'Déjà corrigé', value: 'AlreadyCorrected' , hidden : true},
        { id: 6, title: '', content: 'Déjà suspendus', value: 'AlreadySuspended' , hidden : true},
    ]
    const history = useHistory()
    const [state, setState] = useState({
        RMD_ID: '',
        RMD_DateOperation: '',
        RMD_CodePartenaire: '',
        RMD_NomBatch: '',
        RMD_CompteTechnique: '',
        RMD_CIN: '',
        RMD_MontantVerse: '',
        RMD_AMP_RIM: '',
        RMD_AMP_NomPrenom: '',
        RMD_AMP_RSMRIM: '',
        RMD_AMP_CT: '',
        RMD_AMP_CIN: '',
        RMD_AMP_DateEcheance: '',
        RMD_AMP_RestePayer: '',
        RMD_PAR_RIM: '',
        RMD_PAR_NomPrenom: '',
        RMD_PAR_RSMRIM: '',
        RMD_PAR_CT: '',
        RMD_PAR_CIN: '',
        RMD_PAR_DateEcheance: '',
        RMD_PAR_RestePayer: '',
        RMD_Decision: '',
        RMD_Commentaire: '',


    })

    const useStyles = makeStyles((theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            color: '#fff',
        },
    }),
);
    const [stateUpdate, setStateUpdate] = useState({

        rmD_ID: "",
        rmD_Decision: "",
        rmD_ID_Doublons: "",
        rmD_O_Decision: ""

    })
    //
    const [Decision_updated, setdoub] = useState(null)
    const [disabled, setEnableFieldDoublons] = useState(false)
    const [Decision_updated_Doub, setdoub_Doub] = useState(null)
    const [openSnack, setOpenSnack] = React.useState(false);
    const [openSnackCorrected, setOpenSnackCorrected] = React.useState(false);
    const [openSnackDec, setOpenSnackDec] = React.useState(false);
    const [Comm_Orig, setCommOrig] = useState(null)
    const [Comm_Doub, setCommDoub] = useState(null)
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    const handleChange = (event) => {

        setState({
            ...state,
            [event.target.name]: event.target.value,
        })

    }


    const handleCloseSnak = () => {
        setOpenSnack(false);
    };

    const handleCloseSnakDec = () => {
        setOpenSnackDec(false);
    };

    const handleFormSubmittedd = () => {
        setOpen(true);
        const id = JSON.parse(localStorage.getItem('iD_doublons'));
        const id_doub = JSON.parse(localStorage.getItem('id_doublons_doub'));
        const source = localStorage.getItem('SourceDoublons');
        console.log(id)
        if (Decision_updated == null || (Decision_updated_Doub == null && disabled ==false)) {   setOpen(false) ; setOpenSnackDec(true)   }
        else {
            var users = localStorage.getItem('userConnected') ;
            UpdatbyDoubons(id, Decision_updated, id_doub, Decision_updated_Doub, source, Comm_Orig, Comm_Doub,users).then((response) => {

                console.log("tesssssssssssssst", Decision_updated)
                console.log("tesssssssssssssst", Decision_updated, Decision_updated_Doub)
                console.log({ ...response.data })
                setState({ ...response.data })
                localStorage.removeItem('iD_doublons')
                localStorage.removeItem('id_doublons_doub')
                localStorage.removeItem('SourceDoublons')
                setOpen(false);
                setOpenSnackCorrected(true); 
                history.push('/RM-Doublons-table')


            })
        }
    }
    

    const handleCloseSnakCorrected = () => {
        setOpenSnackCorrected(false);
    };

    const handleChangeCommOrig = (event) => { setCommOrig(event.target.value) }

    const handleChangeCommDoub = (event) => { setCommDoub(event.target.value) }

    const handleChangedec = (event) => {

        setdoub(event.target.value)
    }


    const handleChangedecDoub = (event) => {

        setdoub_Doub(event.target.value)
    }




    const handleFormSubmitted = () => {

        getUserById({
            ...stateUpdate,
        }).then(() => {
            localStorage.setItem('itemgeted', JSON.stringify(getUserById.data))
        })

    }

 
    const rmD_ID = localStorage.getItem('iD_doublons')
    useEffect(() => {

        getUserById(rmD_ID).then((data) => {
            setState({ ...data.data })
            if (data.data.rmD_D_Decision == "BloquedForChange") {
                // var a = 1
                
                setdoub_Doub('SENDED')
                setEnableFieldDoublons(true)
            }else if (data.data.rmD_D_Decision == "SuspendedSend") {
                // var a = 1
                
                setdoub_Doub('SuspendedSENDED')
                setEnableFieldDoublons(true)
            }else if (data.data.rmD_D_Decision == "AlreadyChanged") {
                // var a = 1
                
                setdoub_Doub('AlreadyCorrected')
                setEnableFieldDoublons(true)
            }else if (data.data.rmD_D_Decision == "SuspendedChanged") {
                // var a = 1
                
                setdoub_Doub('AlreadySuspended')
                setEnableFieldDoublons(true)
            }else{
                
              
                setEnableFieldDoublons(false)
            }

            // handleSetStates()
        })
    }, [rmD_ID])



    return (
        <div className="m-sm-30">

      <Backdrop className={classes.backdrop} open={open}>
                <CircularProgress color="inherit" />
            </Backdrop>

            <Snackbar open={openSnack} autoHideDuration={6000} onClose={handleCloseSnak}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnak} severity="error" sx={{ width: '100%' }}>
                    Le Champs Commentaire est Obligatoire !
                </Alert>
            </Snackbar>

            <Snackbar open={openSnackCorrected} autoHideDuration={6000} onClose={handleCloseSnakCorrected}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnakCorrected} severity="success" sx={{ width: '100%' }}>
                    Correction Terminé !
                </Alert>
            </Snackbar>


            <Snackbar open={openSnackDec} autoHideDuration={6000} onClose={handleCloseSnakDec}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnakDec} severity="error" sx={{ width: '100%' }}>
                    Il faut Prendre Une décision !
                </Alert>
            </Snackbar>

            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: 'Doublons', path: '/RM-Doublons-table' },
                        { name: 'Correction des doublons' },
                    ]}
                />
            </div>

            <Card elevation={3}>
                <div className="flex p-4">
                    <h4 className="m-0">Edit_RH_Doublons</h4>
                </div>
                <Divider className="mb-2" />

                <form className="p-4" /*onSubmit={handleFormSubmitted*/>
                    <Grid container spacing={3} alignItems="center">

                        <Grid item md={4} sm={4} xs={12}>
                            Données A Partenaire
                        </Grid>
                        <Grid item md={12} sm={8} xs={16}>

                        <TextField
                                className="m-2"
                                label="Partenaire"
                                onChange={handleChange}
                                name=""
                                size="small"
                                variant="outlined"
                                color="secondary"
                                value={state.rmD_Source}
                                InputProps={{
                                    readOnly: true,
                                }}
                                focused
                            />


                            
                        <TextField
                                className="m-2"
                                label="RIM Client"
                                onChange={handleChange}
                                name=""
                                size="small"
                                variant="outlined"
                                color="secondary"
                                value={state.rmD_RIM}
                                InputProps={{
                                    readOnly: true,
                                }}
                                focused
                            />


                            <TextField
                                className="m-2"
                                label="Agence RIM"
                                onChange={handleChange}
                                name=""
                                size="small"
                                variant="outlined"
                                color="secondary"
                                // value={"610"}
                                InputProps={{
                                    readOnly: true,
                                }}
                                focused
                            />
                            <TextField
                                className="m-2"
                                label="Nom RIM"
                                onChange={handleChange}
                                name=""
                                size="small"
                                variant="outlined"
                                color="secondary"
                                value={state.rmD_FullName}
                                InputProps={{
                                    readOnly: true,
                                }}
                                focused
                            />
                            <TextField
                                className="m-2"
                                label="RSM du RIM"
                                onChange={handleChange}
                                name="rmD_AMP_RSMRIM"
                                size="small"
                                variant="outlined"
                                color="secondary"
                                value={state.rmD_RSM}
                                InputProps={{
                                    readOnly: true,
                                }}
                                focused
                            />




                            <TextField
                                className="m-2"
                                label="CIN"
                                onChange={handleChange}
                                name="rmD_CIN"
                                size="small"
                                variant="outlined"
                                value={state.rmD_CIN}
                                focused
                                InputProps={{
                                    readOnly: true,
                                }}
                            />


                            <TextField
                                className="m-2"
                                label="Compte Technique"
                                onChange={handleChange}
                                name="rmD_CompteTechnique"
                                size="small"
                                variant="outlined"
                                color="secondary"
                                value={state?.rmD_CT}
                                InputProps={{
                                    readOnly: true,
                                }}
                                focused
                            />

                        </Grid>

                        <Grid item md={12} sm={8} xs={10}>

                            <TextField
                                className="m-2"
                                label="Date"
                                name="rmD_AMP_DateEcheance"
                                size="small"
                                variant="outlined"
                                focused

                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rmD_O_DateTransaction}


                                onChange={handleChange}

                            />

                            <TextField
                                className="m-2"
                                label="Montan versé"
                                name="rmD_MontantVerse"
                                size="small"
                                variant="outlined"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rmD_O_MontantVerse}
                                onChange={handleChange}
                                focused

                            />
                            <TextField
                                className="m-2"
                                label="Agence"
                                name=""
                                size="small"
                                variant="outlined"
                                focused
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rmD_O_Agence}
                                onChange={handleChange}
                            />
                            {/* <TextField
                                className="m-2"
                                label="Flux"
                                name="rmD_NomBatch"
                                size="small"
                                variant="outlined"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rmD_NomBatch}
                                onChange={handleChange}
                                focused

                            /> */}
                            <TextField
                                className="m-2"
                                label="Commaintaire"
                                name="rmD_Commentaire"
                                size="small"
                                variant="outlined"
                                focused
                                //value={state?.rmD_Commentaire}
                                onChange={handleChangeCommOrig}
                                focused

                            />

                            <TextField
                                className="m-2 min-w-188"
                                label="Selectionner"
                                name=""
                                size="small"
                                variant="outlined"
                                focused
                                onChange={handleChangedec}
                                value={Decision_updated}
                                select

                            >
                                {selectList.map((item, ind) => (
                                    <MenuItem
                                        value={item.value}
                                        key={item.id}
                                        disabled = {item.value==="SENDED"||  item.value==="SuspendedSENDED" || item.value==="AlreadyCorrected"|| item.value==="AlreadySuspended" ? true : false }
                                    >
                                        {item.content}
                                    </MenuItem>
                                ))}
                            </TextField>

                        </Grid>

                        <Grid item md={12} sm={8} xs={10}>

                            <TextField
                                className="m-2"
                                label="Date"
                                name="RMD_AMP_DateEcheance"
                                size="small"
                                variant="outlined"
                                focused

                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rmD_D_DateTransaction}


                                onChange={handleChange}

                            />

                            <TextField
                                className="m-2"
                                label="Montan versé"
                                name="rmD_MontantVerse"
                                size="small"
                                variant="outlined"
                                focused

                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rmD_D_MontantVerse}
                                onChange={handleChange}
                            />
                            <TextField
                                className="m-2"
                                label="Agence"
                                name=""
                                size="small"
                                variant="outlined"
                                focused

                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rmD_D_Agence}
                                onChange={handleChange}
                            />
                            {/* <TextField
                                className="m-2"
                                label="Flux"
                                name="rmD_NomBatch"
                                size="small"
                                variant="outlined"
                                focused
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rmD_NomBatch}
                                onChange={handleChange}
                            /> */}
                            <TextField
                                className="m-2"
                                label="Commaintaire"
                                name="rmD_Commentaire"
                                size="small"
                                variant="outlined"
                                focused = {!disabled}
                                disabled={disabled}
                                
                                //value={state?.rmD_Commentaire}
                                onChange={handleChangeCommDoub}
                            />

                            {<TextField
                                className="m-2 min-w-188"
                                label="Selection"
                                id='Selection'
                                name="rmD_Decision"

                                size="small"
                                variant="outlined"
                                // focused = {!disabled}
                                focused = {true}
                                // disabled={disabled}
                                InputProps={{ readOnly: disabled }}
                                select
                                value={Decision_updated_Doub}
                                // value={Decision_updated_Doub}
                                onChange={handleChangedecDoub}
                                variant="outlined"
                                

                            >
                                {selectList.map((item, ind) => (
                                    <MenuItem
                                        value={item.value}
                                        key={item.id}
                                        disabled = {item.value==="SENDED"||  item.value==="SuspendedSENDED" || item.value==="AlreadyCorrected"|| item.value==="AlreadySuspended" ? true : false }
                                    >
                                        {item.content}
                                    </MenuItem>
                                ))}
                            </TextField>}
                        </Grid>
                    </Grid>
                    <div class="m-8 " >
                        <Button style={{ left: 500 }}
                            color="primary"
                            variant="contained"
                            // type="submit"

                            onClick={handleFormSubmittedd}
                        >
                            Enregistrer
                        </Button>
                        <Link to="/RM-Doublons-table">
                            <Button style={{ left: 550 }}
                                position="absolute"

                                color='secondary'

                                variant="contained"
                                type="Annuler"
                            // onClick={handleFormSubmit}
                            >
                                Annuler
                            </Button>
                        </Link>
                    </div>
                </form>

            </Card>
        </div>
    )

}

export default RM_DoublonsForm
