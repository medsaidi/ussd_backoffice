import React from 'react'
import { authRoles } from '../../../auth/authRoles'

const RM_DoublonsRoute = [
    {
        path: '/RM-Doublons-table',
        exact: true,
        component: React.lazy(() => import('./RM_DoublonsTable')),
        auth: authRoles.sa,
    },
    {
        path: '/RM-DoublonsForm',
        component: React.lazy(() =>
            import('./RM_DoublonsForm')
        ),
    },
]

export default RM_DoublonsRoute
