import React from 'react'
import { authRoles } from '../../auth/authRoles'

const PartenairesRoute = [
    {
        path: '/Partenaires-table',
        exact: true,
        component: React.lazy(() => import('./PartenairesTable')),
        // auth: authRoles.sa,
    },
    {
        path: '/PartenairesForm',
        component: React.lazy(() =>
            import('./PartenairesForm')
        ),
    },
]

export default PartenairesRoute
