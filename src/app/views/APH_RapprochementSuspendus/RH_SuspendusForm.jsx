import React, { useState, useEffect, setState } from 'react'
import { getAllSuspendus, deleteUser, getUserById, updateSuspendus, postVerificationCohérence } from './RH_SuspendusServices'
import {
    Grid,
    FormControl,
    RadioGroup,
    FormControlLabel,
    Radio,
    Card,
    Divider,
    TextField,
    MenuItem,
    Tabs,
    Tab,
    Button,
    Backdrop

} from '@material-ui/core'
import { Breadcrumb } from 'app/components'
import { makeStyles, createStyles,createMuiTheme , ThemeProvider} from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Slide from '@material-ui/core/Slide'
import { Link } from 'react-router-dom'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { green , yellow } from '@material-ui/core/colors'
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />
})

const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#008000' ,//your color,
      
      } 
    },
   // width : 5,
  });

  const themeYellow = createMuiTheme({
    palette: {
      primary: {
        main: '#ebe538' //your color
      }
    }
  });

const useStyles = makeStyles((theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            color: '#fff',
        },
    }),
);




const RH_SuspendusForm = () => {
    const [state, setState] = useState({
        rdS_ID: '',
        rdS_CIN: '',
        rdS_MontantVerse: '',
        rdS_CT_RIM: '',
        rdS_CT_FullName: '',
        rdS_AMP_RSMRIM: '',
        rdS_CT_CT: '',
        rdS_CT_CIN: '',
        rdS_AMP_DateEcheance: '',
        rdS_CT_ResteCredit: '',
        rdS_CIN_RIM: '',
        rdS_CIN_FullName: '',
        rdS_PAR_RSMRIM: '',
        rdS_CIN_CT: '',
        rdS_CIN_CIN: '',
        rdS_PAR_DateEcheance: '',
        rdS_CIN_ResteCredit: '',
        rdS_Decision: '',
        rdS_Commentaire: '',
        rdS_SAPourcentage: '',
        rdS_CreatedOn: '',
        rdS_CreatedBy: '',
        rdS_UpdatedOn: '',
        rdS_UpdatedBy: '',
        rdS_Ammount: '',
    })




    const [comm, setcomm] = useState(null)
    const [rdS_Decision_updated, setdec] = useState(null)
    const [openDialog, setOpenDialog] = React.useState(false)
    const [openSnack, setOpenSnack] = React.useState(false);
    const [openSnackDec, setOpenSnackDec] = React.useState(false);
    const [openSnackVerif, setOpenSnackVerif] = React.useState(false);
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);


    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    const handleCloseSnak = () => {
        setOpenSnack(false);
    };

    const handleCloseSnakDec = () => {
        setOpenSnackDec(false);
    };

    const handleCloseSnakVerif = () => {
        setOpenSnackVerif(false);
    };


    
    const handleChangecomm = (event) => {
        setcomm(event.target.value);
    }

    const handleChange = (event) => {

        setState({
            ...state,
            [event.target.name]: event.target.value,
        })

    }
    const handleChangedec = (event) => {
        setdec(event.target.value)
    }

    const handleFormSubmitted = () => {
        if (rdS_Decision_updated == null) { setOpenSnackDec(true); }
        else if (comm == null) {
            setOpenSnack(true);
        }
        else {
            setOpen(true);
            const id = JSON.parse(localStorage.getItem('id_sus'));
            const source = localStorage.getItem('SourceSus');
            var users = localStorage.getItem('userConnected') ;
            postVerificationCohérence(state.rdS_CT, state.rdS_CIN)
                .then((verif)=> 
                {
                    if(verif.data){
                        updateSuspendus(id, state.rdS_CIN, state.rdS_CT, comm, rdS_Decision_updated, source,users).then((response) => {
                            console.log(response)
                            setState({ ...response.data })
                            setOpenDialog(true);
                            localStorage.removeItem('id_sus')
                            localStorage.removeItem('SourceSus')
                        })
                    }else{
                        setOpen(false);
                        setOpenSnackVerif(true);
                        console.log('NOTOK : ' + verif.data);
                    }
                })
        }
    }
 
   
    const id_rds = JSON.parse(localStorage.getItem('id_sus'));
    useEffect(() => {

        getUserById(id_rds).then((data) => {
            setState({ ...data.data })
        })
    }, [id_rds])
    return (
        <div className="m-sm-30">
            <Snackbar open={openSnack} autoHideDuration={5000} onClose={handleCloseSnak}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnak} severity="error" sx={{ width: '100%' }}>
                    Le Champs Commentaire est Obligatoire !
                </Alert>
            </Snackbar>

            <Snackbar open={openSnackVerif} autoHideDuration={5000} onClose={handleCloseSnakVerif}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnakVerif} severity="error" sx={{ width: '100%' }}>
                    Le Couple CIN-CT non correspond a aucun Client !
                </Alert>
            </Snackbar>


            <Snackbar open={openSnackDec} autoHideDuration={5000} onClose={handleCloseSnakDec}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnakDec} severity="error" sx={{ width: '100%' }}>
                    Il faut Prendre Une décision !
                </Alert>
            </Snackbar>


            <Backdrop className={classes.backdrop} open={open}>
                <CircularProgress color="inherit" />
            </Backdrop>


            <Dialog
                open={openDialog}
                TransitionComponent={Transition}
                disableBackdropClick
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">
                    {"Correction Terminé"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        La correction est términé avec succès
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Link to="/RH-Suspendus-table">
                        <Button className="m-2 bg-secondary text-white" >
                            Valider !
                        </Button>
                    </Link>

                </DialogActions>
            </Dialog>



            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: 'Suspendus', path: '/RH-Suspendus-table' },
                        { name: 'Rapprochement des suspendus' },
                    ]}
                />
            </div>


            <Card elevation={3}>
                <div className="flex p-4">
                    <h4 className="m-0">Rapprochement des Suspendus</h4>
                </div>
                <Divider className="mb-2" />

                <div className="p-4" >
                    {/* <form className="p-4" onSubmit={handleFormSubmitted} > */}
                    <Grid container spacing={3} alignItems="center">
                        <Grid item md={12} sm={3} xs={6}>
                            Données A Changer
                        </Grid>
                        <Grid item md={12} sm={3} xs={6}>
                        <ThemeProvider theme={themeYellow} >
                            <TextField 
                                className="m-2"
                                label="CIN"
                                onChange={handleChange}
                                name="rdS_CIN"
                                size="small"
                                variant="outlined"
                                value={state.rdS_CIN}
                                focused
                            />
                            </ThemeProvider>
                                   <ThemeProvider theme={theme}>
                            <TextField
                            
                                className="m-2"
                                label="Compte Technique"
                                onChange={handleChange}
                                name="rdS_CT"
                                size="small"
                                variant="outlined"
                               // color="secondary"
                                value={state.rdS_CT}
                                // InputProps={{
                                //     classes: {
                                //       notchedOutline: classess.notchedOutline
                                //     }
                                //   }}
                                focused
                            />
                             </ThemeProvider>
                            <TextField
                                className="m-2"
                                label="Montant versé"
                                name="rdS_MontantVerse"
                                size="small"
                                variant="outlined"
                                //disabled
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state.rdS_Ammount}
                                focused

                            />
                        </Grid>
                        <Grid item md={12} sm={4} xs={6}>
                            Données Relative au CIN
                        </Grid>
                        <Grid item md={12} sm={3} xs={6}>
                            <TextField
                                className="m-2"
                                label="RIM"
                                name="rdS_PAR_RIM"
                                size="small"
                                variant="outlined"
                               // disabled
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state.rdS_CIN_RIM}
                                onChange={handleChange}
                                focused
                            />
                            <ThemeProvider theme={themeYellow}>
                            <TextField
                                className="m-2"
                                label="CIN"
                                name="rdS_AMP_CIN"
                                size="small"
                                variant="outlined"
                                // disabled
                                // InputProps={{
                                //     readOnly: true,
                                // }}
                                value={state?.rdS_CIN_CIN}
                                onChange={handleChange}
                                focused
                            />
                            </ThemeProvider>
                            <TextField
                                className="m-2"
                                label="Compte Technique"
                                name="rdS_AMP_CT"
                                size="small"
                                variant="outlined"
                                color="primary"

                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rdS_CIN_CT}
                                onChange={handleChange}
                                focused
                            />
                            <TextField
                                className="m-2"
                                label="Nom"
                                name="rdS_AMP_NomPrenom"
                                size="small"
                                variant="outlined"
                                //disabled
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rdS_CIN_FullName}
                                onChange={handleChange}
                                focused
                            />

                            <TextField
                                className="m-2"
                                label="Date Echeance"
                                name="rdS_AMP_DateEcheance"
                                size="small"
                                variant="outlined"
                               // disabled
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={new Date(state.rdS_CIN_DateEcheance).toLocaleDateString()}
                                onChange={handleChange}
                                focused
                            />



                            <TextField
                                className="m-2"
                                label="Restant Du"
                                name="rdS_MontantVerse"
                                size="small"
                                variant="outlined"
                                //disabled
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rdS_CIN_ResteCredit}
                                onChange={handleChange}
                                focused
                            />

                        </Grid>
                        <Grid item md={12} sm={4} xs={6}>
                            Données Relative au CT
                        </Grid>
                        <Grid item md={12} sm={3} xs={6}>
                            <TextField
                                className="m-2"
                                label="RIM"
                                name="rdS_AMP_RIM"
                                size="small"
                                variant="outlined"
                                //disabled
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state.rdS_CT_RIM}
                                onChange={handleChange}
                                focused
                            />
                            <TextField
                                className="m-2"
                                label="CIN"
                                name="rdS_PAR_CIN"
                                size="small"
                                color="success"
                                variant="outlined"

                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rdS_CT_CIN}
                                onChange={handleChange}
                                focused
                            />
                             <ThemeProvider theme={theme}>
                            <TextField
                                className="m-2"
                                label="Compte Technique"
                                name="rdS_PAR_CT"
                                size="small"
                                variant="outlined"
                                value={state?.rdS_CT_CT}
                                onChange={handleChange}
                                focused
                            />
                            </ThemeProvider>
                            <TextField
                                className="m-2"
                                label="Nom"
                                name="rdS_PAR_NomPrenom"
                                size="small"
                                variant="outlined"
                                //disabled
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rdS_CT_FullName}
                                onChange={handleChange}
                                focused

                            />


                            <TextField
                                className="m-2"
                                label="Date Echeance"
                                name="rdS_PAR_DateEcheance"
                                size="small"
                                variant="outlined"
                               // disabled
                               InputProps={{
                                readOnly: true,
                            }}
                                value={new Date(state.rdS_CT_DateEcheance).toLocaleDateString()}
                                onChange={handleChange}
                                focused
                            />
                            <TextField
                                className="m-2"
                                label="Restant Du"
                                name="rdS_MontantVerse"
                                size="small"
                                variant="outlined"
                                //disabled
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={state?.rdS_CT_ResteCredit}
                                onChange={handleChange}
                                focused
                            />

                        </Grid>
                        <Grid item md={4} sm={4} xs={6}>
                            Commentaire
                        </Grid>
                        <Grid item md={12} sm={8} xs={6}>
                            <TextField
                                label="Commentaire"
                                multiline
                                name="rdS_Commentaire"
                                value={comm}
                                rows={4}
                                onChange={handleChangecomm}
                                //   style={{ width: 500 }} 
                                fullWidth
                                variant="outlined"
                                required
                            />
                        </Grid>


                        <Grid item md={4} sm={4} xs={6}>
                            Décision
                        </Grid>
                        <Grid item md={12} sm={8} xs={6}>
                            <FormControl component="fieldset">
                                <RadioGroup
                                    row
                                    name="rdS_Decision"
                                    value={rdS_Decision_updated}
                                    onChange={handleChangedec}
                                    defaultValue="select"

                                >
                                    <FormControlLabel
                                        className="h-20 mr-6"
                                        label="Séléctionner"

                                        control={
                                            <Radio
                                                size="small"
                                                color="secondary"
                                            />
                                        }
                                        disabled
                                    />
                                    <FormControlLabel
                                        className="h-20"
                                        label="Valider"
                                        value="CORRECTED"
                                        control={
                                            <Radio
                                                size="small"
                                                color="secondary"
                                            />
                                        }
                                    />
                                    {/* <FormControlLabel
                                                className="h-20"
                                                label="Suspendus"
                                                value="SUSPENDED"
                                                control={
                                                    <Radio
                                                        size="small"
                                                        color="secondary"
                                                    />
                                                }
                                            /> */}
                                </RadioGroup>
                            </FormControl>
                        </Grid>




                    </Grid>


                    <div className="mt-6">
                        <Button
                            className="m-2 bg-green text-white"
                            // color="primary"
                            variant="contained"
                            //type="submit"
                            // onSubmit={handleFormSubmitted()}
                            onClick={handleFormSubmitted}
                        //  onClick={() => {

                        //     handleFormSubmitted();
                        //     // handleClose();
                        //   }}
                        >
                            Enregistrer Et Continuer
                        </Button>
                        <Link to="/RH-Suspendus-table">
                            <Button
                                className="m-2 bg-secondary text-white"
                                variant="contained"
                                type="submit"

                            >
                                Annuler
                            </Button>

                        </Link>
                    </div>
                </div>


            </Card>
        </div>
    )
}

export default RH_SuspendusForm
