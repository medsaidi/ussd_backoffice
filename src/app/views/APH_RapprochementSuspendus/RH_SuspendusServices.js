import axios from 'axios.js'

export const getAllSuspendus = () => {
    return axios.get('/api/Suspendus')
}
export const getAllSuspendusByDate = (date) => {
    return axios.get('/api/Suspendus?DateOp='+date)
}

export const getAllWithFiltre = (date,part) => {
    return axios.post('/api/Suspendus',{rdS_DateTransaction : date , rdS_Source : part })
}


export const getUserById = (id) => {
    return axios.get('/api/Suspendus/'+id, { data: id })
}
export const deleteUser = (User) => {
    return axios.post('/api/user/delete', User)
}
export const addNewUser = (User) => {
    return axios.post('/api/user/add', User)
}
export const updateSuspendus = (ID,CIN,ct,comm,dec,source,user) => {
    return axios.put('/api/Suspendus' , {
        rdS_ID:ID,
        rdS_CT:ct,
        rdS_CIN:CIN,
        rdS_Decision:dec,
        rdS_Commentaire:comm,
        rdS_Source : source,
        rdS_UserModified : user
    })
}
export const postVerificationCohérence = (CT,CIN) => {
    return axios.post('/api/Suspendus/VerificationCohereance',{rdS_CT : CT , rdS_CIN : CIN })
}
