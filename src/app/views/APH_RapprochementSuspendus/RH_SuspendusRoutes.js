import React from 'react'
import { authRoles } from '../../auth/authRoles'

const RH_SuspendusRoute = [
    {
        path: '/RH-Suspendus-table',
        exact: true,
        component: React.lazy(() => import('./RH_SuspendusTable')),
        auth: authRoles.sa,
    },
    {
        path: '/RH-SuspendusForm',
        component: React.lazy(() =>
            import('./RH_SuspendusForm')
        ),
    },
]

export default RH_SuspendusRoute
