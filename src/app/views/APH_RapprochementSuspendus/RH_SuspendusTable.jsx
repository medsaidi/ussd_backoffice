import React, { useState, useEffect } from 'react'
import {
    IconButton,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Icon,
    TablePagination,
    Button,
    Card,
    TextField,
    Grid,
    MenuItem,
} from '@material-ui/core'
import MUIDataTable from 'mui-datatables'
import { getAllSuspendus,  getAllWithFiltre } from './RH_SuspendusServices'
import { Breadcrumb,SimpleCard } from 'app/components'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import { format } from 'date-fns';

const useStyles = makeStyles(({ palette, ...theme }) => ({
    productTable: {
        '& thead': {
            '& th:first-child': {
                paddingLeft: 16,
            },
        },
        '& td': {
            borderBottom: 'none',
        },
        '& td:first-child': {
            paddingLeft: '16px !important',
        },
    },
}))

const RH_SuspendusTable = () => {
    const [uid, setUid] = useState(null)
    const [rowsPerPage, setRowsPerPage] = useState(10)
    const [page, setPage] = useState(0)
    const [user, setUser] = useState(null)
    const [userList, setUserList] = useState([{
    //    rdS_Id : '',
    //    rdS_Source : '',
       
    //    rdS_CT : '',
    //    rdS_CIN :'',
    //    rdS_Ammount: ''


    }
])
    const [selected, setSelected] = useState('');
    const [selectedDate, setDate] = useState(format(new Date(),'yyyy-MM-dd')); 
    const [inputValue, setInputValue] = useState(format(new Date(),'yyyy-MM-dd')); 
    const classes = useStyles()
     

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }


    const onDateChange = (date, value) => {
        setDate(date);
        setInputValue(value);
        localStorage.setItem('DateSelectedSuspendus',JSON.stringify(value))
      };
    

    const updatePageData = () => {
        getAllSuspendus().then(({ data }) => {
            setUserList(data)
        })
    }
    
    const  handleChange = (event)=> {
    setSelected(event.target.value);  
    }

    const getbydate = () =>{

        
        const date=JSON.parse(localStorage.getItem('DateSelectedSuspendus')); 
        console.log(date); 
       
          if (date != null)
      {
             setInputValue(date);
             getAllWithFiltre(date,selected).then(({ data }) => {
              setUserList(data)

           
          })
       }
          else  {
            
              getAllWithFiltre(inputValue,selected).then(({ data }) => {
                  setUserList(data)

                
              })
          }
  
    }

 
    useEffect(() => {

        getbydate()
         localStorage.removeItem('DateSelected')

    }, [])
    return (
        <div className="m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb routeSegments={[{ name: 'Suspendus' }]} />
            </div>
            <SimpleCard title="Critères de recherche">
                <Grid container spacing={3} alignItems="center">
              
                <Grid item md={10} sm={8} xs={12}>
                                    <div className="flex flex-wrap m--2">
                                        <MuiPickersUtilsProvider
                                            utils={DateFnsUtils}
                                        >
                                            <KeyboardDatePicker
                                                className="m-2"
                                                margin="none"
                                                label="Date"
                                                inputVariant="outlined"
                                                type="text"
                                                size="small"
                                                 autoOk={true}
                                                format="yyyy-MM-dd"
                                                // placeholder="format(new Date(),'yyyy-MM-dd')"
                                                 inputValue={inputValue}
                                                onChange={onDateChange}
                                            />
                                        </MuiPickersUtilsProvider>

                                        <TextField
                                            className="m-2 min-w-188"
                                            label="Partenaire"
                                            name="part"
                                            size="small"
                                            variant="outlined"
                                            
                                            value={selected}
                                            onChange={handleChange}
                                            select
                                        >
                                            {paymentTermList.map(
                                                (item, ind) => (
                                                    <MenuItem
                                                        value={item.value}
                                                        key={item.id}
                                                    >
                                                        {item.content}
                                                    </MenuItem>
                                                )
                                            )}
                                        </TextField>

                                        <Button
                                        className="m-2 bg-green text-white"
                                         variant="contained"
                                        //  color="bg-green"
                                         onClick={() => getbydate()}
                                         >
                                          Chercher
                                        </Button>
                                         
                                    </div>
                                </Grid>

                         
                            </Grid>                        



            </SimpleCard>
            <br />
            <Card className="w-full overflow-auto" elevation={6}>
              
                <MUIDataTable
                title={'Liste Suspendus'}
                data={userList}
                 columns={columns}
                options={{
                    filterType: 'textField',
                    responsive: 'simple',
                    selectableRows: 'none',  
                    fixedHeader : true,
                      pagination: true,  
                      viewColumns: false , 
                    elevation: 0,
                    rowsPerPageOptions: [10, 20, 40, 80, 100],
                    onRowClick: (rowData) => {
                        console.log(rowData[0] , rowData[1]);
                        localStorage.setItem('id_sus',rowData[0])
                        localStorage.setItem('SourceSus',rowData[1])
                       
                     },
                    
                }}
                /> 

            </Card>
           
        </div>
    )
}
const paymentTermList = [
     {id :0 , title:'' ,content :'Tous'  , value: ''},
     {id :1 ,title:'', content :'AMEN' , value :'AMEN'},
     {id :2,title:'', content :'AWB' , value :'AWB'},
]

const columns = [
    {
        name: "rdS_ID",
        label: "Id",
        options: {
          display: false,
          filter: false,
          sort: false,

        }
      },
      {
        name: "rdS_Source",
        label: "Source",
        options: {
            filter: true,
        }
      },
    {
         name: 'rdS_DateTransaction', // field name in the row object
        label: 'Date Transaction', // column title that will be shown in table
       
        options: {
            filter: true,
            customBodyRender: (value) => new Date(value).toLocaleDateString()
        },
        
       
        
      
                 
    },
     
    {
        name: 'rdS_CT', // field name in the row object
        label: 'CT', // column title that will be shown in table
        options: {
            filter: true,
        },
    },
    {
        name: 'rdS_CIN', // field name in the row object
        label: 'CIN', // column title that will be shown in table
        options: {
            filter: true,
        },
    },
 
    {
        name: 'rdS_Ammount', // field name in the row object
        label: 'Montant', // column title that will be shown in table
        options: {
            filter: true,
        },
    },
    {
        name: "Action",
        options: {
          filter: false,
          sort: false,
          empty: true,
          customBodyRenderLite: () => {
              
            return (
                <Link to="/RH-SuspendusForm"> 
              <IconButton>
               <Icon color="primary">edit</Icon>
              </IconButton>
              </Link>
            );
          }
        }
      },
     
]
export default RH_SuspendusTable
