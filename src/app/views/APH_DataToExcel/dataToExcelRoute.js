import React from 'react'
import { authRoles } from '../../auth/authRoles'

const dataToExcelRoute = [
    {
        path: '/dataToExcel-table',
        exact: true,
        component: React.lazy(() => import('./DataToExcel')),
        auth: authRoles.sa,
    },
]

export default dataToExcelRoute
