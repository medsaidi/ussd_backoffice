import axios from 'axios.js'

export const getAllSuspendus = (date,part,date_fin) => {
    return axios.post('/api/monitoring/GetSuspendus',{dateOperation : date, dateOperation_Fin :date_fin, codePartenaire : part })
}

export const getAllReco = (date,part,date_fin) => {
    return axios.post('/api/monitoring/GetReco',{dateOperation : date , dateOperation_Fin : date_fin, codePartenaire : part })
}

export const getAllRejets = (date,part,date_fin) => {
    return axios.post('/api/monitoring/GetRejt',{dateOperation : date , dateOperation_Fin : date_fin, codePartenaire : part })
}

export const getAllSuccess = (date,part,date_fin) => {
    return axios.post('/api/monitoring/GetSuccess',{dateOperation : date ,dateOperation_Fin : date_fin , codePartenaire : part })
}

export const getAllRompus = (date,part,date_fin) => {
    return axios.post('/api/monitoring/GetRompus',{dateOperation : date , dateOperation_Fin :date_fin, codePartenaire : part })
}

export const getAllSuspendusHis = (date,part,date_fin) => {
    return axios.post('/api/monitoring/GetSuspendusHis',{dateOperation : date , dateOperation_Fin :date_fin, codePartenaire : part })
}

export const getAllEcheance = (date,part,date_fin) => {
    return axios.post('/api/monitoring/GetEpf',{dateOperation : date , dateOperation_Fin :date_fin, codePartenaire : part })
}