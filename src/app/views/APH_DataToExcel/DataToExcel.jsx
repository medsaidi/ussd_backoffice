import React, { useState, useEffect } from 'react'
import {
    Grid, Card, IconButton, Icon,
    TextField,
    MenuItem,
    Backdrop,
} from '@material-ui/core'
import CircularProgress from '@material-ui/core/CircularProgress'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import { Breadcrumb, SimpleCard } from 'app/components'
import { getAllSuspendus, getAllReco, getAllRejets, getAllSuccess, getAllRompus, getAllSuspendusHis, getAllEcheance } from './DataToExcelServices'
import { ExportToExcel } from 'ExportToExcel'
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import { format } from 'date-fns';


const useStyles = makeStyles((theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            color: '#fff',
        },
    }),
);


const DataToExcel = () => {
    const statList = [
        {
            icon: 'people',
            title: 'Liste Suspendus',
            id: 1,
        },
        {
            icon: 'people',
            title: 'Liste Reconciliation',
            id: 2,
        },
        {
            icon: 'people',
            title: 'Liste Rejet',
            id: 3,
        },
        {
            icon: 'people',
            title: 'Liste Success',
            id: 4,
        },
        {
            icon: 'people',
            title: 'Liste Rompus',
            id: 5,
        },
        {
            icon: 'people',
            title: 'Liste Suspendus Corriger',
            id: 6,
        },
        {
            icon: 'people',
            title: 'Liste Echeance',
            id: 7,
        },
    ]
    const fileName = "Suspendus";
    const fileNameReconciliation = "Reconciliation";
    const fileNameRejet = "Rejets";
    const fileNameSucces = "Succes";
    const fileNameRompus = "Rompus";
    const fileNameHis = "Historique_Suspendus";
    const fileNameEch = "Echeance";
    const [userListexcel, setUserListexcel] = useState([])
    const [userListexcelReco, setUserListexcelReco] = useState([])
    const [userListexcelRejet, setUserListexcelRejet] = useState([])
    const [userListexcelSucc, setUserListexcelSucc] = useState([])
    const [userListexcelRompus, setUserListexcelRompus] = useState([])
    const [userListexcelHis, setUserListexcelHis] = useState([])
    const [userListexcelEch, setUserListexcelEch] = useState([])
    const [open, setOpen] = React.useState(false);
    const [selected, setSelected] = useState('');
    const [selectedDate, setDate] = useState(format(new Date(), 'yyyy-MM-dd'));
    const [inputValue, setInputValue] = useState(format(new Date(), 'yyyy-MM-dd'));
    const [selectedDateFin, setDateFin] = useState(format(new Date(), 'yyyy-MM-dd'));
    const [inputValueFin, setInputValueFin] = useState(format(new Date(), 'yyyy-MM-dd'));

    const classes = useStyles();





    const onDateChangeFin = (date, value) => {

        setDateFin(date);
        setInputValueFin(value);

        localStorage.setItem('datefin', JSON.stringify(value))
        updatePageDataReco();
        updatePageDataRejets();
        updatePageDataSuspendus();
        updatePageDataSuspendedHis();
        updatePageDataEcheance();
        updatePageDataRompus()
        updatePageDataSucces();
    }

    const handleChange = (event) => {

        setSelected(event.target.value);
        localStorage.setItem('partenaire', JSON.stringify(event.target.value))
        console.log(localStorage.getItem('partenaire'))
        updatePageDataReco();
        updatePageDataRejets();
        updatePageDataSuspendus();
        updatePageDataRompus();
        updatePageDataSuspendedHis();
        updatePageDataEcheance();
        updatePageDataSucces();
        setOpen(false);
    }

    const onDateChange = (date, value) => {

        setDate(date);
        setInputValue(value);
        localStorage.setItem('datedebut', JSON.stringify(value))
        updatePageDataReco();
        updatePageDataRejets();
        updatePageDataSuspendus();
        updatePageDataRompus();
        updatePageDataSuspendedHis();
        updatePageDataEcheance();
        updatePageDataSucces();


    };



    const updatePageDataReco = () => {
        const date_deb = JSON.parse(localStorage.getItem('datedebut'))
        const date_fin = JSON.parse(localStorage.getItem('datefin'))
        const part = JSON.parse(localStorage.getItem('partenaire'))
        getAllReco(date_deb, part, date_fin).then((response) => {
            setOpen(true);
            let newArray = [];
            response.data.map((item) => {
                let obj = {
                    Source: item.trC_Partenaire, CT: item.trC_CT, CIN: item.trC_CIN,
                    Date_Operation: format(new Date(item.trC_DateOperation), 'yyyy-MM-dd'), Date_Transaction: format(new Date(item.trC_DateInsertion), 'yyyy-MM-dd')
                    ,
                    //  Date_Operation :  format(new Date(item.trC_DateOperation),'yyyy-MM-dd') ,
                    Montant: item.trC_Amount,
                    //  MontantRestant : item.trC_RestAmount,
                    StatutTransaction: item.trC_StatutTransaction,
                    DueDate: format(new Date(item.trC_DueDate), 'yyyy-MM-dd'), FullName: item.trC_FullName
                };
                newArray.push(obj);
                setUserListexcelReco(newArray)
                console.log('1')

            });


        })
    }

    const updatePageDataSuspendus = () => {
        const date_deb = JSON.parse(localStorage.getItem('datedebut'))
        const date_fin = JSON.parse(localStorage.getItem('datefin'))
        const part = JSON.parse(localStorage.getItem('partenaire'))
        getAllSuspendus(date_deb, part, date_fin).then((response) => {
            let newArray = [];
            response.data.map((item) => {
                let obj = {
                    Source: item.trS_Partenaire, Agence: item.trS_Agence, Libelle: item.trS_LIBELLE,
                    DateOperation: format(new Date(item.trS_DateDiffusion), 'yyyy-MM-dd'), Montant: item.trS_Montant,
                    Date_Transaction_APH: format(new Date(item.trS_DateTransaction), 'yyyy-MM-dd')
                };
                newArray.push(obj);
                setUserListexcel(newArray)
                // console.log('3')
                setOpen(false);
            });

        })
    }


    const updatePageDataSuspendedHis = () => {
        const date_deb = JSON.parse(localStorage.getItem('datedebut'))
        const date_fin = JSON.parse(localStorage.getItem('datefin'))
        const part = JSON.parse(localStorage.getItem('partenaire'))
        getAllSuspendusHis(date_deb, part, date_fin).then((response) => {
            let newArray = [];
            response.data.map((item) => {
                let obj = {
                    Source: item.hsC_CodePArtenaire, CT: item.hsC_Ct, CT_Rectifie: item.hsC_CtRectifie,
                    CIN: item.hsC_CIN, CIN_Rectifie: item.hsC_CINRectifie,
                    //Nouvelle_Status : item.hsC_StatutTransaction ,
                    Montant: item.hsC_Montant,
                    Date_Operation: format(new Date(item.hsC_DateOperation), 'yyyy-MM-dd'),
                    Date_Transaction: format(new Date(item.hsC_DateInsertion), 'yyyy-MM-dd'),
                    Date_Suspension: format(new Date(item.hsC_DateSuspension), 'yyyy-MM-dd'), Nom_Agent_Suspension: item.hsC_NomSuspension,
                    Date_Rectification: format(new Date(item.hsC_DateRectification), 'yyyy-MM-dd'), Nom_Agent_Rectification: item.hsC_NomBackOffice
                };
                newArray.push(obj);
                setUserListexcelHis(newArray)
                // console.log('5')
                setOpen(false);
            });

        })
    }

    const updatePageDataRejets = () => {
        const date_deb = JSON.parse(localStorage.getItem('datedebut'))
        const date_fin = JSON.parse(localStorage.getItem('datefin'))
        const part = JSON.parse(localStorage.getItem('partenaire'))
        getAllRejets(date_deb, part, date_fin).then((response) => {
            let newArray = [];
            response.data.map((item) => {
                let obj = {
                    ClientID: item.trR_RIM, Partenaire: item.trR_Partner, GSM: item.trR_GSM,
                    "Date d'écheance": format(new Date(item.trR_DueDate), 'yyyy-MM-dd'), "Date D'opération": format(new Date(item.trR_DateInsertion), 'yyyy-MM-dd'),
                    Montant_Echeance: item.trR_DueAmount, ResteApayer: item.trR_RestAmount, CT: item.trR_CT, CIN: item.trR_CIN,
                    "Motif de rejet": item.trR_Comment, Agence: item.trR_Agency
                };
                newArray.push(obj);
                setUserListexcelRejet(newArray)
                // console.log('2')
                setOpen(false);
            });

        })
    }

    const updatePageDataRompus = () => {
        const date_deb = JSON.parse(localStorage.getItem('datedebut'))
        const date_fin = JSON.parse(localStorage.getItem('datefin'))
        const part = JSON.parse(localStorage.getItem('partenaire'))
        getAllRompus(date_deb, part, date_fin).then((response) => {
            let newArray = [];
            response.data.map((item) => {
                let obj = {
                    CIN: item.reR_CIN, CT: item.reR_CT, MontantVerser: item.reR_MontantVerser,
                    NomComplet: item.reR_FullName, DueDate: format(new Date(item.reR_DueDate), 'yyyy-MM-dd'), Description: item.reR_Description,
                    Motifs: item.reR_Motif, DateCreation: format(new Date(item.reR_Creartion), 'yyyy-MM-dd'), DateTransaction: format(new Date(item.reR_DateTransaction), 'yyyy-MM-dd')
                };
                newArray.push(obj);
                setUserListexcelRompus(newArray)
                // console.log('4')
                setOpen(false);
            });

        })
    }



    const updatePageDataEcheance = () => {
        const date_deb = JSON.parse(localStorage.getItem('datedebut'))
        const date_fin = JSON.parse(localStorage.getItem('datefin'))
        const part = JSON.parse(localStorage.getItem('partenaire'))
        getAllEcheance(date_deb, part, date_fin).then((response) => {
            let newArray = [];
            response.data.map((item) => {
                let obj = {
                    "Canal de remboursement ": item.epF_Partenaire, Id_client: item.epF_ClientId, CT: item.epF_CT, CIN: item.epF_CIN,
                    Date_Echeance: format(new Date(item.epF_DateEchance), 'yyyy-MM-dd'), Montant: item.epF_MontantEcheance,
                };
                newArray.push(obj);
                setUserListexcelEch(newArray);
                setOpen(false);
            });
            // localStorage.removeItem('datedebut')
            // localStorage.removeItem('datefin')
            // localStorage.removeItem('partenaire')
        })
    }

    const updatePageDataSucces = () => {
        const date_deb = JSON.parse(localStorage.getItem('datedebut'))
        const date_fin = JSON.parse(localStorage.getItem('datefin'))
        const part = JSON.parse(localStorage.getItem('partenaire'))
        getAllSuccess(date_deb, part, date_fin).then((response) => {
            let newArray = [];
            response.data.map((item) => {
                let obj = {
                    Source: item.trT_Partenaire, Agence: item.trT_Agence, CT: item.trT_CT, CIN: item.trT_CIN,
                    Date_Transaction: format(new Date(item.trT_DateInsertion), 'yyyy-MM-dd'), Date_Operation: format(new Date(item.trT_DateTransaction), 'yyyy-MM-dd'),
                    Montant: item.trT_Amount,
                    FullName: item.trT_FullName, Date_Echeance: format(new Date(item.trT_DueDate), 'yyyy-MM-dd')
                };
                newArray.push(obj);
                setUserListexcelSucc(newArray)

            });
            // localStorage.removeItem('datedebut')
            // localStorage.removeItem('datefin')
            // localStorage.removeItem('partenaire')
            setOpen(false);
        })
    }
    useEffect(() => {

        localStorage.setItem('datefin', JSON.stringify(format(new Date(), 'yyyy-MM-dd')))
        localStorage.setItem('datedebut', JSON.stringify(format(new Date(), 'yyyy-MM-dd')))
        localStorage.setItem('partenaire', JSON.stringify(""))
        updatePageDataReco();
        updatePageDataRejets();
        updatePageDataSuspendus();
        updatePageDataRompus();
        updatePageDataSuspendedHis();
        updatePageDataEcheance();
        updatePageDataSucces();
        console.log(localStorage.getItem('datefin'))
        console.log(localStorage.getItem('datedebut'))
    }, [])

    return (

        <div className="analytics m-sm-30">
            <Backdrop className={classes.backdrop} open={open}>
                <CircularProgress color="inherit" />
            </Backdrop>


            <div className="flex justify-between items-center items-center mb-6">
                <Breadcrumb routeSegments={[{ name: 'Exporting Data' }]} />
            </div>

            <SimpleCard title="Critères de recherche">
                <Grid container spacing={3} alignItems="center">

                    <Grid item md={10} sm={8} xs={12}>
                        <div className="flex flex-wrap m--2">
                            <MuiPickersUtilsProvider
                                utils={DateFnsUtils}
                            >
                                <KeyboardDatePicker
                                    className="m-2"
                                    margin="none"
                                    label="Date"
                                    inputVariant="outlined"
                                    type="text"
                                    size="small"
                                    autoOk={true}
                                    format="yyyy-MM-dd"
                                    // placeholder="format(new Date(),'yyyy-MM-dd')"
                                    inputValue={inputValue}
                                    onChange={onDateChange}
                                />
                            </MuiPickersUtilsProvider>



                            <MuiPickersUtilsProvider
                                utils={DateFnsUtils}
                            >
                                <KeyboardDatePicker
                                    className="m-2"
                                    margin="none"
                                    label="Date"
                                    inputVariant="outlined"
                                    type="text"
                                    size="small"
                                    autoOk={true}
                                    format="yyyy-MM-dd"
                                    // placeholder="format(new Date(),'yyyy-MM-dd')"
                                    inputValue={inputValueFin}
                                    onChange={onDateChangeFin}
                                />
                            </MuiPickersUtilsProvider>


                            <TextField
                                className="m-2 min-w-188"
                                label="Partenaire"
                                name="part"
                                size="small"
                                variant="outlined"

                                value={selected}
                                onChange={handleChange}
                                select
                            >
                                {paymentTermList.map(
                                    (item, ind) => (
                                        <MenuItem
                                            value={item.value}
                                            key={item.id}
                                        >
                                            {item.content}
                                        </MenuItem>
                                    )
                                )}
                            </TextField>





                            {/* <Button
                                        className="m-2 bg-green text-white"
                                         variant="contained"
                                        //  color="bg-green"
                                         onClick={() => getbydate()}
                                         >
                                          Chrercher
                                        </Button> */}

                        </div>
                    </Grid>


                </Grid>



            </SimpleCard>
            <br />


            <Grid container spacing={3}>
                {statList.map((item, ind) => (
                    <Grid key={item.title} item md={3} sm={6} xs={12}>
                        <Card elevation={3} className="p-5 flex">
                            <div>
                                <IconButton
                                    size="small"
                                    className="p-2 bg-light-gray"
                                >
                                    <Icon className="text-muted">
                                        {item.icon}
                                    </Icon>
                                </IconButton>
                            </div>
                            <div className="ml-4">
                                <h3 className="mt-1 text-32">
                                    {/* {item.amount.toLocaleString()}  */}
                                    {(() => {
                                        switch (item.id) {
                                            case 1: return <ExportToExcel apiData={userListexcel} fileName={fileName} />;
                                            case 2: return <ExportToExcel apiData={userListexcelReco} fileName={fileNameReconciliation} />;
                                            case 3: return <ExportToExcel apiData={userListexcelRejet} fileName={fileNameRejet} />;
                                            case 4: return <ExportToExcel apiData={userListexcelSucc} fileName={fileNameSucces} />;
                                            case 5: return <ExportToExcel apiData={userListexcelRompus} fileName={fileNameRompus} />;
                                            case 6: return <ExportToExcel apiData={userListexcelHis} fileName={fileNameHis} />;
                                            case 7: return <ExportToExcel apiData={userListexcelEch} fileName={fileNameEch} />;
                                        }
                                    })()}
                                </h3>
                                <p className="m-0 text-muted">{item.title}</p>
                            </div>
                        </Card>
                    </Grid>
                ))}
            </Grid>





        </div>


    )
}

const paymentTermList = [
    { id: 0, title: '', content: 'Tous', value: '' },
    { id: 1, title: '', content: 'AMEN', value: 'AMEN' },
    { id: 2, title: '', content: 'AWB', value: 'Attijeri' },
    { id: 2, title: '', content: 'ViaMobile', value: 'ViaMobile' },
    { id: 2, title: '', content: 'Poste', value: 'Poste' },
    { id: 2, title: '', content: 'UIB', value: 'UIB' },
]

export default DataToExcel
