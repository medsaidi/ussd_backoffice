import React, { useState, useEffect } from 'react'
import {
    Card,
    TextField,
    Grid,
    FormControlLabel,
    Switch,
    Typography,
    Button,
} from '@material-ui/core'
import { getClientInfos, resetPIN } from './ClientsServices'
import { Breadcrumb, SimpleCard } from 'app/components'
import MUIDataTable from 'mui-datatables'
import { useLocation, useHistory } from 'react-router-dom'

const ClientInfosTable = () => {
    const location = useLocation()
    const history = useHistory()

    const [clientInfos, setClientInfos] = useState([])
    const [id, setId] = useState(0)
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [amplitudeId, setAmplitudeId] = useState('')

    useEffect(() => {
        if (!location.state) {
            history.push('/Clients-table')
            return
        }

        const { id, firstName, lastName, amplitudeId } = location.state
        setId(id)
        setFirstName(firstName)
        setLastName(lastName)
        setAmplitudeId(amplitudeId)

        getClientInfos(id)
            .then(({ data }) => {
                setClientInfos(data)
            })
            .catch((error) => {
                history.push('/Clients-table')
            })
    }, [history, location.state])

    const [isLoading, setIsLoading] = useState(false)

    const reset = (id) => {
        setIsLoading(true)

        resetPIN(id).then((response) => {
            setIsLoading(response)
        })
    }
    const columns = [
        {
            name: 'mobile',
            label: 'Mobile',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'accountLabel',
            label: 'Account Label',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'accountNumber',
            label: 'Account Number',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'branch',
            label: 'Branch',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'dateCreated',
            label: 'Date Created',
            options: {
                filter: true,
                sort: true,
                customBodyRender: (value) =>
                    new Date(value).toLocaleDateString(),
            },
        },
        {
            name: 'status',
            label: 'Status',
            options: {
                filter: true,
                sort: true,
                customBodyRenderLite: (dataIndex) => {
                    const value = clientInfos[dataIndex]
                    const text = value.status ? 'Active' : 'Inactive'
                    return (
                        <Typography
                            className="m-2"
                            variant="body1"
                            color="textPrimary"
                        >
                            {text}
                        </Typography>
                    )
                },
            },
        },
        {
            name: 'alias',
            label: 'Alias',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'action',
            label: 'Action',
            options: {
                filter: true,
                sort: true,
                customBodyRenderLite: (dataIndex) => {
                    const value = clientInfos[dataIndex]
                    return (
                        <div className="flex items-center">
                            <FormControlLabel
                                className="my-5"
                                control={<Switch checked={value.action} />}
                            />
                        </div>
                    )
                },
            },
        },
    ]
    return (
        <div className="m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb routeSegments={[{ name: 'Client Information' }]} />
            </div>
            <SimpleCard title="1-Client Information">
                <Grid container spacing={3} alignItems="center">
                    <Grid item md={10} sm={8} xs={12}>
                        <div className="flex flex-wrap m--2">
                            <TextField
                                className="m-2"
                                label="First Name"
                                name="firstName"
                                size="small"
                                variant="outlined"
                                color="primary"
                                value={firstName}
                            />
                            <TextField
                                className="m-2"
                                label="Last Name"
                                name="lastName"
                                size="small"
                                variant="outlined"
                                color="primary"
                                value={lastName}
                            />
                            <TextField
                                className="m-2 min-w-188"
                                label="AmplitudeID"
                                name="amplitudeID"
                                size="small"
                                variant="outlined"
                                value={amplitudeId}
                            />
                        </div>
                    </Grid>
                </Grid>
            </SimpleCard>
            <br />
            <Card className="w-full overflow-auto" elevation={6}>
                <MUIDataTable
                    title={''}
                    data={clientInfos}
                    columns={columns}
                    options={{
                        filterType: 'textField',
                        responsive: 'simple',
                        selectableRows: 'none',
                        fixedHeader: true,
                        pagination: true,
                        viewColumns: false,
                        elevation: 0,
                        rowsPerPageOptions: [10, 20, 40, 80, 100],
                    }}
                />
            </Card>
            <br />
            <SimpleCard title="2-PIN">
                <Grid container spacing={3} justify="center">
                    <Grid item md={10} sm={8} xs={12}>
                        <div className="flex flex-wrap justify-center">
                            <Button
                                className="m-2 bg-green text-white"
                                variant="contained"
                                onClick={() => reset(id)}
                                disabled={isLoading === true}
                            >
                                {isLoading === true
                                    ? ' Reset PIN...'
                                    : ' Reset PIN'}
                            </Button>
                        </div>
                    </Grid>
                </Grid>
            </SimpleCard>
        </div>
    )
}

export default ClientInfosTable
