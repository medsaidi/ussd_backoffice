import React from 'react'
import { authRoles } from '../../auth/authRoles'

const ClientInfoRoute = [
    {
        path: '/Client-Info',
        exact: true,
        component: React.lazy(() => import('./ClientInfos')),
        auth: authRoles.sa,
    },
]

export default ClientInfoRoute
