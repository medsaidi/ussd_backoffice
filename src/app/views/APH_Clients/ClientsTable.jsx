import React, { useState, useEffect } from 'react'
import {
    Button,
    Card,
    MenuItem,
    Grid,
    TextField,
    Typography,
} from '@material-ui/core'
import { getClients, getAllBranch, exportClients } from './ClientsServices'
import { Breadcrumb, SimpleCard } from 'app/components'
import { useHistory } from 'react-router-dom'
import MUIDataTable from 'mui-datatables'

const ClientTable = () => {
    const [myData, setMyData] = useState([])
    useEffect(() => {
        getAllBranch()
            .then((response) => {
                setMyData(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
        getClients().then(({ data }) => {
            setClientList(data)
        })
    }, [])
    const [clientList, setClientList] = useState([])
    const [branch, setBranch] = useState('')
    const [id, setId] = useState('')
    const [phone, setPhone] = useState('')
    const handleBranchChange = (event) => {
        setBranch(event.target.value)
    }
    const handleIdChange = (event) => {
        setId(event.target.value)
    }
    const handlePhoneChange = (event) => {
        setPhone(event.target.value)
    }
    const getWithFilter = () => {
        getClients(id, phone, branch).then(({ data }) => {
            setClientList(data)
        })
    }
    const history = useHistory()
    const handleActionClick = (id, firstName, lastName, amplitudeId) => {
        const params = {
            id: id,
            firstName: firstName,
            lastName: lastName,
            amplitudeId: amplitudeId,
        }
        history.push({
            pathname: '/Client-Info',
            state: params,
        })
    }
    const columns = [
        {
            name: 'amplitudeId',
            label: 'AmplitudeId	',
            options: {
                filter: true,
                sort: true,
            },
        },

        {
            name: 'firstName',
            label: 'FirstName',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'lastName',
            label: 'LastName',
            options: {
                filter: true,
                sort: true,
            },
        },

        {
            name: 'phoneNumber',
            label: 'PhoneNumber',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'branch',
            label: 'Branch',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'dateCreated',
            label: 'DateCreated',
            options: {
                filter: true,
                sort: true,
                customBodyRender: (value) =>
                    new Date(value).toLocaleDateString(),
            },
        },
        {
            name: 'ussd',
            label: 'USSD ?',
            options: {
                filter: true,
                sort: true,
                customBodyRenderLite: (dataIndex) => {
                    const value = clientList[dataIndex]
                    const text = value.ussd ? 'Active' : 'Inactif'
                    return (
                        <Typography
                            className="m-2"
                            variant="body1"
                            color="textPrimary"
                        >
                            {text}
                        </Typography>
                    )
                },
            },
        },

        {
            name: 'mobileMoney',
            label: 'Mobile Money ?',
            options: {
                filter: true,
                sort: true,
            },
        },
        {
            name: 'action',
            label: ' ',
            options: {
                filter: true,
                sort: true,
                customBodyRenderLite: (dataIndex) => {
                    let client = clientList[dataIndex]
                    return (
                        <div className="flex items-center">
                            <div className="flex-grow"></div>

                            <Button
                                className="m-2 bg-green text-white"
                                variant="contained"
                                onClick={() =>
                                    handleActionClick(
                                        client.id,
                                        client.firstName,
                                        client.lastName,
                                        client.amplitudeId
                                    )
                                }
                            >
                                more
                            </Button>
                        </div>
                    )
                },
            },
        },
    ]

    return (
        <div className="m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb routeSegments={[{ name: 'Clients' }]} />
            </div>
            <SimpleCard title="Critères de recherche">
                <Grid container spacing={3} alignItems="center">
                    <Grid item md={10} sm={8} xs={12}>
                        <div className="flex flex-wrap m--2">
                            <TextField
                                className="m-2"
                                label="Amplitude ID"
                                onChange={handleIdChange}
                                name="amplitudeID"
                                size="small"
                                variant="outlined"
                                color="primary"
                                value={id}
                            />
                            <TextField
                                className="m-2"
                                label="Phone"
                                onChange={handlePhoneChange}
                                name="phone"
                                size="small"
                                variant="outlined"
                                color="primary"
                                value={phone}
                            />
                            <TextField
                                className="m-2 min-w-188"
                                label="Branche"
                                name="branch"
                                size="small"
                                variant="outlined"
                                value={branch}
                                onChange={handleBranchChange}
                                select
                            >
                                <MenuItem value="">- Tout -</MenuItem>
                                {myData.map((item) => (
                                    <MenuItem
                                        value={item}
                                        key={item}
                                        align="center"
                                    >
                                        {item}
                                    </MenuItem>
                                ))}
                            </TextField>

                            <Button
                                className="m-2 bg-green text-white"
                                variant="contained"
                                onClick={() => getWithFilter()}
                            >
                                Chercher
                            </Button>
                            <Button
                                className="m-2 bg-green text-white"
                                variant="contained"
                                onClick={() => exportClients(id, phone, branch)}
                            >
                                Export
                            </Button>
                        </div>
                    </Grid>
                </Grid>
            </SimpleCard>

            <br />
            <Card className="w-full overflow-auto" elevation={6}>
                <MUIDataTable
                    title={'Clients'}
                    data={clientList}
                    columns={columns}
                    options={{
                        filterType: 'textField',
                        responsive: 'simple',
                        selectableRows: 'none',
                        fixedHeader: true,
                        pagination: true,
                        viewColumns: false,
                        elevation: 0,
                        rowsPerPageOptions: [10, 20, 40, 80, 100],
                    }}
                />
            </Card>
        </div>
    )
}

export default ClientTable
