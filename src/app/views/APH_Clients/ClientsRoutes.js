import React from 'react'
import { authRoles } from '../../auth/authRoles'

const ClientsTableRoute = [
    {
        path: '/Clients-table',
        exact: true,
        component: React.lazy(() => import('./ClientsTable')),
        auth: authRoles.sa,
    },
]

export default ClientsTableRoute
