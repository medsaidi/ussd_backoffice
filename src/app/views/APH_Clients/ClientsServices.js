import axios from 'axios.js'

export const getClients = (id, phone, branch) => {
    return axios.post('/api/Client', {
        AmplitudeId: id,
        Phone: phone,
        Branch: branch,
    })
}
export const getAllBranch = () => {
    return axios.get('/api/Client/GetAllBranch')
}
export const getClientInfos = (idClient) => {
    return axios.post('/api/Client/ClientInfos', { idClient: idClient })
}
export const resetPIN = (idClient) => {
    return axios.post('/api/Client/ResetPIN', { idClient: idClient })
}
export const exportClients = (id, phone, branch) => {
    axios({
        method: 'post',
        url: '/api/Client/ExportClients',
        responseType: 'blob',
        data: {
            AmplitudeId: id,
            Phone: phone,
            Branch: branch,
        },
    })
        .then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]))
            const link = document.createElement('a')
            link.href = url
            link.setAttribute('download', 'ListClients.xlsx')
            document.body.appendChild(link)
            link.click()
        })
        .catch((error) => console.log(error))
}
