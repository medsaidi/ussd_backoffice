import React from 'react'
import { authRoles } from '../../auth/authRoles'

const crudRoute = [
    {
        path: '/crud-table',
        exact: true,
        component: React.lazy(() => import('./CrudTable')),
        auth: authRoles.sa,
    },
]

export default crudRoute
