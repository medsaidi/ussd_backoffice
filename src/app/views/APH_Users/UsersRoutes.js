import React from 'react'
import { authRoles } from '../../auth/authRoles'

const UsersRoute = [
    {
        path: '/Users-table',
        exact: true,
        component: React.lazy(() => import('./UsersTable')),
        // auth: authRoles.sa,
    },
    {
        path: '/UsersForm',
        component: React.lazy(() =>
            import('./UsersForm')
        ),
    },
]

export default UsersRoute
