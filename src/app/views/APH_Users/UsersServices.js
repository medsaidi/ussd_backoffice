import axios from 'axios.js'

export const getAllUser = () => {
    return axios.post('/api/authenticate/GetUsers')
}
export const addUser = (user) => {
    return axios.post('/api/authenticate/register', { 
        registerModel: user,
        email :user.email,
        username : user.username,
        phone : user.phone
    
    })
}
export const deleteUser = (User) => {
    return axios.post('/api/user/delete', User)
}
export const addNewUser = (User) => {
    return axios.post('/api/user/add', User)
}
export const updateUser = (User) => {
    return axios.post('/api/user/update', User)
}
