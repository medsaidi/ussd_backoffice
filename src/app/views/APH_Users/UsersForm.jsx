import React, { useState } from 'react'
import { Formik } from 'formik'
import {
    Grid,
    FormControl,
    RadioGroup,
    FormControlLabel,
    Radio,
    Card,
    Divider,
    TextField,
    MenuItem,
    Tabs,
    Tab,
    Button,
    Backdrop
} from '@material-ui/core'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import { Link } from 'react-router-dom'
import { Breadcrumb } from 'app/components'
import {addUser } from './UsersServices'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


const UsersForm = () => {
 
    const [state, setState] = useState({
        username :'',
        email :'',
        phone :''
    })


    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    
const useStyles = makeStyles((theme) =>
createStyles({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
}),
);
const classes = useStyles();
const [open, setOpen] = React.useState(false);
const [openSnack, setOpenSnack] = React.useState(false);
    const handleChange = (event) => {

        setState({
            ...state,
            [event.target.name]: event.target.value,
        })

    }

    const handleCloseSnak = () => {
        setOpenSnack(false);
    };

    const handleFormSubmitted = () => {
       setOpen(true);
        addUser(state).then((response) => {
        console.log(response)
          setOpen(false);
          setOpenSnack(true);
          setState({username:'',email:'',phone:''})
    })
    }

    return (
        <div className="m-sm-30">

<Snackbar open={openSnack} autoHideDuration={5000} onClose={handleCloseSnak}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <Alert onClose={handleCloseSnak} severity="success" sx={{ width: '100%' }}>
                    Utilisateur Créer !
                </Alert>
            </Snackbar>


                    <Backdrop className={classes.backdrop} open={open}>
                <CircularProgress color="inherit" />
            </Backdrop>

            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: 'Users', path: '/Users-table' },
                        { name: 'Edit_Users' },
                    ]}
                />
            </div>

            <Card elevation={3}>
                <div className="flex p-4">
                    <h4 className="m-0">Edit_Users</h4>
                </div>
                <Divider className="mb-2" />

                
                        <form className="p-4"  >
                            <Grid container spacing={3} alignItems="center">
                                <Grid item md={2} sm={4} xs={12}>
                                    Role Utilisateur
                                </Grid>
                                <Grid item md={10} sm={8} xs={12}>
                                    <FormControl component="fieldset">
                                        <RadioGroup
                                            row
                                            name="customerType"
                                            //value={values.customerType}
                                            onChange={handleChange}
                                        >
                                            <FormControlLabel
                                                className="h-20 mr-6"
                                                label="Admin"
                                                value="Admin"
                                                control={
                                                    <Radio
                                                        size="small"
                                                        color="secondary"
                                                    />
                                                }
                                            />
                                            <FormControlLabel
                                                className="h-20"
                                                label="User"
                                                value="User"
                                                control={
                                                    <Radio
                                                        size="small"
                                                        color="secondary"
                                                    />
                                                }
                                            />
                                        </RadioGroup>
                                    </FormControl>
                                </Grid>

                                <Grid item md={2} sm={4} xs={12}>
                                   User Name
                                </Grid>
                                <Grid item md={10} sm={8} xs={12}>
                                    <div className="flex flex-wrap m--2">
                                        
                                        <TextField
                                            className="m-2"
                                            label="User Name"
                                            name="username"
                                            size="small"
                                            variant="outlined"
                                            value={state.username}
                                            onChange={handleChange}
                                        />
                                    </div>
                                </Grid>

                               

                                
                              

                                <Grid item md={2} sm={4} xs={12}>
                                      Email
                                </Grid>
                                <Grid item md={10} sm={8} xs={12}>
                                    <TextField
                                        label="Email"
                                        name="email"
                                        size="small"
                                        type="email"
                                        variant="outlined"
                                       value={state.email}
                                        onChange={handleChange}
                                    />
                                </Grid>

                                <Grid item md={2} sm={4} xs={12}>
                                      Phone
                                </Grid>
                                <Grid item md={10} sm={8} xs={12}>
                                    <div className="flex flex-wrap m--2">
                                        <TextField
                                            className="m-2"
                                            label="Phone"
                                            name="phone"
                                            size="small"
                                            variant="outlined"
                                          value={state.phone}
                                            onChange={handleChange}
                                        />
                                        
                                    </div>
                                </Grid>

                               
                             </Grid>
                            

                            <div className="mt-6">
                        <Button
                            className="m-2 bg-green text-white"
                            
                            variant="contained"
                            
                         onClick={() => {

                             handleFormSubmitted();
                            // handleClose();
                          }}
                        >
                            Enregistrer 
                        </Button>
                        <Link to="/Users-table">
                            <Button
                                className="m-2 bg-secondary text-white"
                                variant="contained"
                                type="submit"

                            >
                                Annuler
                            </Button>

                        </Link>
                    </div>
                        </form>
                  
            </Card>
        </div>
    )
}

 
 

const initialValues = {
    customerType: '',
}

export default UsersForm
