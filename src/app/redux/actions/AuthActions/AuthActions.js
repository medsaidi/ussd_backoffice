import axios from 'axios'
import firebase from 'firebase/app'
import 'firebase/auth'
export const AUTH_LOGIN = 'AUTH_LOGIN'
export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS'

const getDataSuccess = (data) => {
    return {
        type: GET_DATA_SUCCESS,
        data: data
    }
}
export const postAuthLogin = (email, password) => (dispatch) => {
    const username = {username : email};
        axios
        .post(`http://localhost:5000/api/authenticate/login`, { username: email,password })
        .then(res => {
            getDataSuccess(res.response.data);
        //   history.push("/");
        })
        .catch(err => {
          if (err.response.status === 401) {
          }
        });
    // firebase.auth().signInWithEmailAndPassword()
    // axios.post('/api/authenticate/login').then((res) => {
    //     dispatch({
    //         type: AUTH_LOGIN,
    //         payload: res.data,
    //     })
    // })
}
