import React, { createContext, useEffect, useReducer } from 'react'
import axios from 'axios'
import firebase from 'firebase/app'
import 'firebase/auth'
import { firebaseConfig } from 'config.js'
import { MatxLoading } from 'app/components'
import * as auth from 'app/redux/actions/AuthActions/AuthActions'
export const AUTH_LOGIN = 'AUTH_LOGIN'
export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS'

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
}

const initialAuthState = {
    isAuthenticated: false,
    isInitialised: false,
    user: null,
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'FB_AUTH_STATE_CHANGED': {
            const { isAuthenticated, user } = action.payload

            return {
                ...state,
                isAuthenticated,
                isInitialised: true,
                user,
            }
        }
        default: {
            return { ...state }
        }
    }
}

const AuthContext = createContext({
    ...initialAuthState,
    method: 'FIREBASE',
    createUserWithEmailAndPassword: () => Promise.resolve(),
    signInWithEmailAndPassword: () => Promise.resolve(),
    signInWithGoogle: () => Promise.resolve(),
    logout: () => Promise.resolve(),
})
const getDataSuccess = (data) => {
    return {
        type: GET_DATA_SUCCESS,
        data: data
    }
}
export const AuthProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialAuthState)

    const signInWithEmailAndPassword = (email, password) => {

        // axios.post('http://localhost:5000/api/authenticate/login',{email,password})
        // .then(response => {
        //     dispatch(getDataSuccess(response.data));
        // })
        // .catch(error => {
        //     //TODO: handle the error when implemented
        // })
    //     const article = { username: email,password };
    //     const requestOptions = {
    //         method: 'POST',
    //         headers: { 'Content-Type': 'application/json' },
    //         body: article
    //     };
        
    // axios.post('http://localhost:5000/api/authenticate/login', requestOptions)
    //     .then(response => this.setState(getDataSuccess(response.data)))
    //     .catch(exp => {
    //         console.log(exp)
    //     });
    const username = {username : email};
        axios
        .post(`http://localhost:5000/api/authenticate/login`, { username, password })
        .then(res => {
            this.setState(getDataSuccess(res.response.data))
        //   history.push("/");
        })
        .catch(err => {
          if (err.response.status === 401) {
          }
        });
        // return auth.postAuthLogin(email, password)
        // return postAuthLogin(email, password)
        // return firebase.auth().signInWithEmailAndPassword(email, password)
    }

    const signInWithGoogle = () => {
        const provider = new firebase.auth.GoogleAuthProvider()

        return firebase.auth().signInWithPopup(provider)
    }

    const createUserWithEmailAndPassword = async (email, password) => {
        return firebase.auth().createUserWithEmailAndPassword(email, password)
    }

    const logout = () => {
        return firebase.auth().signOut()
    }

    useEffect(() => {
        const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                dispatch({
                    type: 'FB_AUTH_STATE_CHANGED',
                    payload: {
                        isAuthenticated: true,
                        user: {
                            id: user.uid,
                            name: user.displayName || user.email,
                            avatar: user.photoURL,
                            email: user.email,
                        },
                    },
                })
            } else {
                dispatch({
                    type: 'FB_AUTH_STATE_CHANGED',
                    payload: {
                        isAuthenticated: false,
                        user: null,
                    },
                })
            }
        })

        return unsubscribe
    }, [dispatch])

    if (!state.isInitialised) {
        return <MatxLoading />
    }

    return (
        <AuthContext.Provider
            value={{
                ...state,
                method: 'FIREBASE',
                createUserWithEmailAndPassword,
                signInWithEmailAndPassword,
                signInWithGoogle,
                logout,
            }}
        >
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContext
